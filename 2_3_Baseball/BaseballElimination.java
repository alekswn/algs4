import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FordFulkerson;

import java.util.TreeMap;
import java.util.LinkedList;

public class BaseballElimination {
    private class Team {
        private String name;
        private int wins;
        private int losses;
        private int left;
        private int[] games;
        private boolean eliminated;
        private Iterable<String> certificateOfElimination;
        public Team() { };
    }
    
    private final Team[] teams;
    private final TreeMap<String, Integer> teamIndex;
    private final int gamesLeft;
    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename)
    {
        // Read file
        if (filename == null)
                throw new IllegalArgumentException("null argument");
        In infile = new In(filename);
        String line = infile.readLine();
        if (line == null)
            throw new IllegalArgumentException("Invalid input file format");
        final int teamsNumber = Integer.parseInt(line);

        teams = new Team[teamsNumber];
        teamIndex = new TreeMap<String, Integer>();
        int maxWins = 0;
        int leader = -1;
        int gl = 0;
        
        for (int i = 0; i < teamsNumber; i++) {
            Team team  = new Team();
            line = infile.readLine();
            if (line == null)
                throw new IllegalArgumentException("Invalid input file format");
            String[] columns = line.trim().split(" +");
            team.name   = columns[0];
            teamIndex.put(team.name, i);
            team.wins   = Integer.parseInt(columns[1]);
            if (team.wins > maxWins) {
                maxWins = team.wins;
                leader  = i;
            } 
            team.losses = Integer.parseInt(columns[2]);
            team.left   = Integer.parseInt(columns[3]);
            gl = gl + team.left;
//            StdOut.println("Debug: " + team.left + " " + gl);
            team.games  = new int[teamsNumber];
            for (int j = 0; j < teamsNumber; j++)
                team.games[j] = Integer.parseInt(columns[4 + j]);
            teams[i] = team;
        }
        gamesLeft = gl;
        
        //Probe for elimination
        for (int i = 0; i < teamsNumber; i++) {
            Team team = teams[i];
            if (team.wins + team.left < maxWins) { //trivial elimination
                team.eliminated = true;
                LinkedList<String> cert = new LinkedList<String>();
                cert.push(teams[leader].name);
                team.certificateOfElimination = cert;
            } else { // might be non-trivial elimination
                Iterable<String> cert = probeNonTrivialElimination(i);
                if (cert != null) {
                    team.eliminated = true;
                    team.certificateOfElimination = cert;
                } else {
                    team.eliminated = false;
                    team.certificateOfElimination = null;
                }
            }
        }
        
    }
    // number of teams
    public int numberOfTeams()
    {
        return teams.length;
    }
    // all teams
    public Iterable<String> teams()
    {
        return teamIndex.keySet();
    }
    // number of wins for given team
    public              int wins(String team)
    {
        if (!teamIndex.containsKey(team))
            throw new IllegalArgumentException("Unknown team: " + team);
        return teams[teamIndex.get(team)].wins;
    }
    // number of losses for given team
    public              int losses(String team)
    {
        if (!teamIndex.containsKey(team))
            throw new IllegalArgumentException("Unknown team: " + team);
        return teams[teamIndex.get(team)].losses;
    }
    // number of remaining games for given team
    public              int remaining(String team)
    {
        if (!teamIndex.containsKey(team))
            throw new IllegalArgumentException("Unknown team: " + team);
        return teams[teamIndex.get(team)].left;
    }
    // number of remaining games between team1 and team2
    public              int against(String team1, String team2)
    {
        if (!teamIndex.containsKey(team1))
            throw new IllegalArgumentException("Unknown team: " + team1);
        if (!teamIndex.containsKey(team2))
            throw new IllegalArgumentException("Unknown team: " + team2);
        return teams[teamIndex.get(team1)].games[teamIndex.get(team2)];
    }
    // is given team eliminated?
    public          boolean isEliminated(String team)
    {
        if (!teamIndex.containsKey(team))
            throw new IllegalArgumentException("Unknown team: " + team);
        return teams[teamIndex.get(team)].eliminated;
    }
    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team)
    {
        if (!teamIndex.containsKey(team))
            throw new IllegalArgumentException("Unknown team: " + team);
        return teams[teamIndex.get(team)].certificateOfElimination;
    }
    
    private Iterable<String> probeNonTrivialElimination(int index) {
        //Calculate layout of the flow network
        final int N = numberOfTeams();                 //total number of teams
        final int source = 0;                          //index of virtual source
        final int firstGame = source + 1;              //beginning of game's column
        final int gamesNum  = ((N - 2) * (N - 1)) / 2; //height of game's column
        final int firstTeam = firstGame + gamesNum;    //beginning of team's column
        final int teamsNum  = N - 1;                   //height of team's column
        final int target = firstTeam + teamsNum;       //index of virtual target
        final int networkSize = target + 1;            //number of vertixes
        //Create flow network
        FlowNetwork net = new FlowNetwork(networkSize);

        int k = firstTeam;
        int maxFlow = 0;
        for (int i = 0; i < N; i++) {
            if (i == index) continue;
            int flow = teams[index].wins + teams[index].left - teams[i].wins;
            assert (flow >= 0);
            maxFlow = maxFlow + flow;
            net.addEdge(new FlowEdge(k++, target, flow));
        }
        assert (k == target);
        k = firstGame;
        for (int i = 0; i < N; i++) {
            if (i == index) continue;
            for (int j = i + 1; j < N; j++) {
                if (j == index) continue;
                net.addEdge(new FlowEdge(0, k, teams[i].games[j]));
                int t1 = firstTeam + i;
                if (i > index) t1--;
                net.addEdge(new FlowEdge(k, t1, maxFlow));
                int t2 = firstTeam + j;
                if (j > index) t2--;
                net.addEdge(new FlowEdge(k, t2, maxFlow));
                k++;
            }
        }
        assert (k == firstTeam);

        FordFulkerson ff = new FordFulkerson(net, source, target);

        LinkedList<String> result = new LinkedList<String>();
        for (int i = firstTeam; i < target; i++) {
            if (ff.inCut(i)) {
                int teamIdx = i - firstTeam;
                if (teamIdx >= index) teamIdx++;
                result.push(teams[teamIdx].name);
            }
        }
        if (result.isEmpty()) return null;
        return result;
    }

    public static void main(String[] args) {
    BaseballElimination division = new BaseballElimination(args[0]);
    for (String team : division.teams()) {
        if (division.isEliminated(team)) {
            StdOut.print(team + " is eliminated by the subset R = { ");
            for (String t : division.certificateOfElimination(team)) {
                StdOut.print(t + " ");
            }
            StdOut.println("}");
        }
        else {
            StdOut.println(team + " is not eliminated");
        }
    }
}
}

import edu.princeton.cs.algs4.StdOut;
import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> { //16 bytes

   private Node first;                               //8 bytes
   private Node last;                                //8 bytes
   private int size;                                 //8 bytes

   private class Node {                              // 24 bytes
      private Item item;                             //8 bytes
      private Node next;                             //8 bytes
      private Node prev;                             //8 bytes

      public Node() { //Time: C
         item = null;
         next = null;
         prev = null;
      }
      public Node(Item i, Node p, Node n) { //Time: C
         if (i == null) throw new NullPointerException();
         item = i;
         next = n;
         prev = p;
         prev.next = this;
         next.prev = this;
      }
      public Item pop() {
         next.prev = prev;
         prev.next = next;
         next = null;
         prev = null;
         return item;
      }
      public boolean hasNext() { //Time: C
         return (next.item != null);
      }
      public boolean hasPrev() { //Time: C
         return (prev.item != null);
      }
      public boolean hasItem() { //Time: C
         return (item != null);
      }
   }

// Memory: 136 + 48N

   private class ForwardIterator implements Iterator<Item> { //24 bytes

      private Node current = first;                          //8 bytes

      public boolean hasNext() { return current.hasNext(); }    //~ C
      public void remove() { throw new UnsupportedOperationException(); }
      public Item next() {  // ~C
         current = current.next;
         if (!current.hasItem()) throw new java.util.NoSuchElementException();
         return current.item;
      }
   }
      

   // construct an empty deque
   public Deque() { //Time : C 
      first = new Node();
      last  = new Node();
      first.next = last;
      last.prev  = first;
      size = 0;
   }
   // is the deque empty?
   public boolean isEmpty() { //Time : C 
      return (size() == 0);
   }
   // return the number of items on the deque
   public int size() { // Time : C
      return size;
   }
   // add the item to the front
   public void addFirst(Item item) { // Time : C
      new Node(item, first, first.next);
      size++;
   }
   // add the item to the end
   public void addLast(Item item) { // Time : C
      new Node(item, last.prev, last);
      size++;
   }
   // remove and return the item from the front
   public Item removeFirst() {  // Time : C
      if (size <= 0) throw new java.util.NoSuchElementException();
      size--;
      return first.next.pop();
   }
   // remove and return the item from the end
   public Item removeLast() { // Time : C
      if (size <= 0) throw new java.util.NoSuchElementException();
      size--;
      return last.prev.pop();
   }
   // return an iterator over items in order from front to end
   public Iterator<Item> iterator() { // Time : C
      return new ForwardIterator();
   }
   // unit testing
   public static void main(String[] args) {
      Deque<Integer> deque;
      int test_counter = 0;
      StdOut.println("Deque : Build-In test has begun");

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : Constructor");
      deque = new Deque<Integer>();
      StdOut.println("[passed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : isEmpty()");
      if (deque.isEmpty()) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : size()");
      if (deque.size() == 0) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : addFirst()");
      deque.addFirst(1);
      if (!deque.isEmpty() && deque.size() == 1) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());
      
      StdOut.print("Deque : Unit Test " + ++test_counter  +" : addLast()");
      deque.addLast(2);
      if (!deque.isEmpty() && deque.size() == 2) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : Iterator");
      Iterator<Integer> it = deque.iterator();
      if (it.hasNext() && it.next() == 1 
         && it.hasNext() && it.next() == 2
         && !it.hasNext()) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : removeFirst()");
      if (deque.removeFirst() == 1 && !deque.isEmpty() && deque.size() == 1) 
         StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());

      StdOut.print("Deque : Unit Test " + ++test_counter  +" : removeLast()");
      if (deque.removeLast() == 2 && deque.isEmpty() && deque.size() == 0) 
         StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(deque.dump());
   }
   
   private String dump() { // Time : N
      StringBuilder result = new StringBuilder();
      for (Node n = first.next; n.hasItem(); n = n.next)
         result.append(n.item + " ");
      return result.toString();
   }
}

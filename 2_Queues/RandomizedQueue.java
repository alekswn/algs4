import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> { //16 bytes

   private Pool pool;                        //8 bytes

   private class Pool {                      //24 bytes
      private Item[] data;                   //24 + 8*length
      private int N;                         //4 bytes

      public Pool() { //~ const
         data = allocateData(1);
         N = 0;
      }
      public void push(Item item) { //~ aconst
         if (data.length == N) resize();
         int r = StdRandom.uniform(++N);
         data[N-1] = data[r];
         data[r] = item;
      }
      public Item pop() { //~ aconst
         if (data.length == 4*N) resize();
         Item res = data[--N];
         data[N] = null;
         return res;
      }
      public Item top() { //~ const
         return data[N-1];
      }
      public Item sample() { //~ const
         return data[StdRandom.uniform(N)];
      }
      public int size() { //~ const
         return N;
      }
      private Item[] allocateData(int sz) { //~ const
         return (Item[]) new Object[sz];
      }
      private void resize() { //~ linear
         Item[] newdata = allocateData(2*N);
         for (int i = 0; i < N; i++) {
            newdata[i] = data[i];
         }
         data = newdata;
      }
   }

// Memory: <= 76 + 16*N

   private class ForwardIterator implements Iterator<Item> { //24 bytes

      private int[] idx;                                     //24 + 8*N
      private int pos;                                       //8 bytes

      public ForwardIterator() { //~ 2*N
         idx = new int[pool.N];
         for (int i = 0; i < idx.length; i++) idx[i] = i;
         StdRandom.shuffle(idx);
         pos = 0;
      }
      public boolean hasNext() { return pos < idx.length; }    //~ const
      public void remove() { throw new UnsupportedOperationException(); }
      public Item next() { // ~const
         if (pos >= idx.length) throw new java.util.NoSuchElementException();
         return pool.data[idx[pos++]];
      }
   }
      
   // construct an empty deque
   public RandomizedQueue() { //~const
      pool = new Pool();
   }
   // is the deque empty?
   public boolean isEmpty() { //~ const 
      return (size() == 0);
   }
   // return the number of items on the deque
   public int size() { //~ const
      return pool.size();
   }
   // add the item
   public void enqueue(Item item) { //~ cM
      if (item == null) throw new NullPointerException();
      pool.push(item);
   }
   // remove and return a random item
   public Item dequeue() {          //~ cM
      if (isEmpty()) throw new java.util.NoSuchElementException();
      return pool.pop();
   }
   // return (but do not remove) a random item
   public Item sample() {           //~ cM
      if (isEmpty()) throw new java.util.NoSuchElementException();
      return pool.sample();
   }
   // return an iterator over items in order from front to end
   public Iterator<Item> iterator() { //~ N
      return new ForwardIterator();
   }

   // unit testing
   public static void main(String[] args) {
      RandomizedQueue<Integer> raque;
      int test_counter = 0;
      StdOut.println("RandomizedQueue : Build-In test has begun");

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  
               +" : Constructor");
      raque = new RandomizedQueue<Integer>();
      StdOut.println("[passed]");
      StdOut.println(raque.dump());

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : isEmpty()");
      if (raque.isEmpty()) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : size()");
      if (raque.size() == 0) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : enqueue()");
      raque.enqueue(1);
      if (raque.size() == 1) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());
      
      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : sample()");
      if (raque.sample() == 1) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : Iterator");
      Iterator<Integer> it = raque.iterator();
      if (it.hasNext() && it.next() == 1 
         && !it.hasNext()) StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());

      StdOut.print("RandomizedQueue : Unit Test " + ++test_counter  +" : dequeue()");
      if (raque.dequeue() == 1 && raque.isEmpty()) 
         StdOut.println("[passed]");
      else StdOut.println("[failed]");
      StdOut.println(raque.dump());

   }
   
   private String dump() { // ~ 4*N
      StringBuilder result = new StringBuilder();
      for (int i = 0; i < pool.data.length; i++)
         result.append(pool.data[i] + " ");
      return result.toString();
   }
}

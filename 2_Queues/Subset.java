import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdRandom;

public class Subset {
   public static void main(String[] args) {
   int k = Integer.parseInt(args[0]);
   int N = 0;
   RandomizedQueue<String> strque = new RandomizedQueue<String>();
   while (!StdIn.isEmpty() && ++N <= k) strque.enqueue(StdIn.readString());
   while (!StdIn.isEmpty()) {
      String str = StdIn.readString();
      if (StdRandom.bernoulli((double) k/N++)) {
         strque.dequeue();
         strque.enqueue(str);
      }
   }
   while (k-- > 0) StdOut.println(strque.dequeue());
   }
}

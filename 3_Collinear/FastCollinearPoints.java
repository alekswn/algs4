//import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Queue;
import java.util.Iterator;
import java.util.Arrays;

public class FastCollinearPoints {
   private static final int MIN_POINTS_NUM = 4;
   private Queue<LineSegment> segs;
   // finds all line segments containing MIN_POINTS_NUM or more points
   public FastCollinearPoints(Point[] points) //~ N^2logN
   {
      segs = new Queue<LineSegment>();
      if (points == null) throw new NullPointerException();
      if (points.length < MIN_POINTS_NUM)
      {
         // check dublicates
         for (int i = 0; i < points.length; i++)
            for (int j = 0; j < points.length; j++)
               if (i != j && points[i].compareTo(points[j]) == 0)
                  throw new IllegalArgumentException();
         return;
      }
      Point[] aux = Arrays.copyOf(points, points.length);

      for (int i = 0; i < points.length; i++)
      {
         Arrays.sort(aux, points[i].slopeOrder());
//         StdOut.println(aux[0]);
         if (aux[0].slopeTo(aux[1]) == Double.NEGATIVE_INFINITY)
            throw new IllegalArgumentException();
         int c = 0;
         for (int j = 1; j < aux.length; j++)
         {
//            StdOut.println("\t" + aux[0].slopeTo(aux[j]) + " " + j + " " + c);
            if (j < aux.length - 1 
                  && aux[0].slopeTo(aux[j]) == aux[0].slopeTo(aux[j+1])) c++;
            else {
               if (c >= (MIN_POINTS_NUM - 2)) {
                  Arrays.sort(aux, j - c, j + 1);
//                  StdOut.print("! ");
//                  for (int t = j - c; t <= j; t++) StdOut.print(aux[t] + " "); 
//                  StdOut.println();
                  if (aux[0].compareTo(aux[j-c]) < 0) 
                     segs.enqueue(new LineSegment(aux[0], aux[j]));
               }
               c = 0;
            }
         }
      }
   }
   // the number of line segments
   public int numberOfSegments()
   {
      return segs.size();
   }
   // the line segments
   public LineSegment[] segments()
   {
      LineSegment[] seg_arr = new LineSegment[segs.size()];
      Iterator<LineSegment> it = segs.iterator();
      for (int i = 0; i < seg_arr.length; i++)  seg_arr[i] = it.next();
      return seg_arr;
   }
 
}

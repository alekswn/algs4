/******************************************************************************
 *  Compilation:  javac Point.java
 *  Execution:    java Point
 *  Dependencies: none
 *  
 *  An immutable data type for points in the plane.
 *  For use on Coursera, Algorithms Part I programming assignment.
 *
 ******************************************************************************/

import java.util.Comparator;
import java.util.Arrays;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;


public class Point implements Comparable<Point> {

    private final int x;     // x-coordinate of this point
    private final int y;     // y-coordinate of this point

    private class BySlope implements Comparator<Point>
    {
        public int compare(Point v, Point w)
        {
            Double v_slope = new Double(slopeTo(v));
            return v_slope.compareTo(slopeTo(w));
        }
    }

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
    public Point(int x, int y) {
        assert (x >= 0 && x <= 32767);
        assert (y >= 0 && y <= 32767);
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    /**
     * Draws this point to standard draw.
     */
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertcal;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
    public double slopeTo(Point that) {
        int dx = that.x - x;
        int dy = that.y - y;
        if (dx == 0) 
        {
            if (dy == 0) return Double.NEGATIVE_INFINITY;
            else return Double.POSITIVE_INFINITY;
        }
        if (dy == 0) return +0.0;
        return (double) dy/dx;
    }

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
    public int compareTo(Point that) {
        int dx = that.x - x;
        int dy = that.y - y;
        if (dy == 0) return -dx;
        return -dy;
    }

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
    public Comparator<Point> slopeOrder() {
        return new BySlope();
    }


    /**
     * Returns a string representation of this point.
     * This method is provide for debugging;
     * your program should not rely on the format of the string representation.
     *
     * @return a string representation of this point
     */
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    /**
     * Unit tests the Point data type.
     */
    public static void main(String[] args) {
        int test_counter = 0;
        boolean test_failed = true;
        StdOut.println("Point : Build-In test has begun");

        StdOut.print("Point : Unit Test " + ++test_counter  +" : Constructor ");
        Point[] points = 
            { new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, 1),
              new Point(1, 2)};
        StdOut.println("[passed]");
        for (Point p : points) {
            StdOut.print(p + " ");
           //p.draw();
        }
        StdOut.println();

        StdOut.print("Point : Unit Test " + ++test_counter  +" : CompareTo ");
        if (
            points[0].compareTo(points[1]) < 0 
            && points[1].compareTo(points[2]) < 0
            && points[3].compareTo(points[2]) < 0
            && points[0].compareTo(points[0]) == 0
            && points[2].compareTo(points[0]) > 0
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }

        StdOut.print("Point : Unit Test " + ++test_counter  +" : SlopeTo ");
        if (
            points[0].slopeTo(points[1]) == +0.0 
            && points[0].slopeTo(points[2]) == 1.0
            && points[0].slopeTo(points[4]) == 2.0
            && points[0].slopeTo(points[3]) == Double.POSITIVE_INFINITY
            && points[0].slopeTo(points[0]) == Double.NEGATIVE_INFINITY
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }

        StdOut.print("Point : Unit Test " + ++test_counter  +" : SlopeOrder ");
        Arrays.sort(points, points[0].slopeOrder());
        if (points[0].slopeTo(points[0]) <= points[0].slopeTo(points[1])
            && points[0].slopeTo(points[1]) <= points[0].slopeTo(points[2])
            && points[0].slopeTo(points[2]) <= points[0].slopeTo(points[3])
            && points[0].slopeTo(points[3]) <= points[0].slopeTo(points[4])
            )
            test_failed = false;
                if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }

    }
}

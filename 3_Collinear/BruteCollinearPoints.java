import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;
import java.util.Iterator;

public class BruteCollinearPoints {
   private Queue<LineSegment> segs;
   // finds all line segments containing 4 points
   public BruteCollinearPoints(Point[] points) //~N^4
   {
      segs = new Queue<LineSegment>();
      if (points == null) throw new NullPointerException();
      // check dublicates
      for (int i = 0; i < points.length; i++)
         for (int j = 0; j < points.length; j++)
            if (i != j && points[i].compareTo(points[j]) == 0)
               throw new IllegalArgumentException();
      if (points.length < 4) return;
      if (points[0] == null || points[1] == null || points[2] == null) 
         throw new NullPointerException();
      for (int i = 0; i < points.length-3; i++)
      {
         Point p = points[i];
         Stack<Point> b = new Stack<Point>();
         Stack<Point> e = new Stack<Point>();
         b.push(p);
         e.push(p);
//         StdOut.println(p);
         for (int j = i+1; j < points.length-2; j++)
         {
            Point q = points[j];
            double pq = p.slopeTo(q);
//            StdOut.println("\t" + q + " " + pq);
            if (pq == Double.NEGATIVE_INFINITY) 
               throw new IllegalArgumentException();
            boolean bpush1 = false;
            boolean epush1 = false;
            if (b.peek().compareTo(q) > 0) {
               b.push(q);
               bpush1 = true;
            } else if (e.peek().compareTo(q) < 0) {
               e.push(q);
               epush1 = true;
            }
            for (int k = j+1; k < points.length-1; k++)
            {
               Point r = points[k];
               double qr = q.slopeTo(r);
//               StdOut.println("\t\t" + r + " " + qr + " " + (pq == qr));
               if (qr == Double.NEGATIVE_INFINITY) 
                  throw new IllegalArgumentException();
               if (pq != qr) continue;
               boolean bpush2 = false;
               boolean epush2 = false;
               if (b.peek().compareTo(r) > 0) {
                  b.push(r);
                  bpush2 = true;
               } else if (e.peek().compareTo(r) < 0) {
                   e.push(r);
                   epush2 = true;
                }
               for (int l = k+1; l < points.length; l++)
               {
                  Point s = points[l];
                  if (s == null) throw new NullPointerException();
                  double rs = r.slopeTo(s);
//                  StdOut.println("!\t\t\t" + s + " " + rs + " " + (qr==rs));
                  if (rs == Double.NEGATIVE_INFINITY) 
                     throw new IllegalArgumentException();
                  if (qr == rs)
                  {
                     Point beg = b.peek();
                     Point end = e.peek();
                     if (beg.compareTo(s) > 0) beg = s;
                     else if (end.compareTo(s) < 0) end = s;
                     segs.enqueue(new LineSegment(beg, end));
                  }
               }
               if (bpush2) b.pop();
               else if (epush2) e.pop();
            }
            if (bpush1) b.pop();
            else if (epush1) e.pop();
         }
      }
   }
   // the number of line segments
   public int numberOfSegments()
   {
      return segs.size();
   }
   // the line segments
   public LineSegment[] segments()
   {
      LineSegment[] seg_arr = new LineSegment[segs.size()];
      Iterator<LineSegment> it = segs.iterator();
      for (int i = 0; i < seg_arr.length; i++)  seg_arr[i] = it.next();
      return seg_arr;
   }
 
}

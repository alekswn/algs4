import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.Stopwatch;

public class PercolationStats {

   private final double timeElapsed;
   private double[] results;

   // perform T independent experiments on an N-by-N grid
   public PercolationStats(int N, int T)
   {
      if (N <= 0 || T <= 0)
         throw new IllegalArgumentException();
      
      results = new double[T];

      Stopwatch stopwatch = new Stopwatch();
      while (T-- > 0) makeExperiment(N, T);
      timeElapsed = stopwatch.elapsedTime();
   }
   // sample mean of percolation threshold
   public double mean()
   {
      return StdStats.mean(results);
   }
   // sample standard deviation of percolation threshold
   public double stddev()
   {
      return StdStats.stddev(results);
   }
   // low  endpoint of 95% confidence interval
   public double confidenceLo()
   {
      return mean() - 1.96*stddev()/Math.sqrt(results.length);
   }
   // high endpoint of 95% confidence interval
   public double confidenceHi()
   {
      return mean() + 1.96*stddev()/Math.sqrt(results.length);
   }
   // perform an individual experiment and collect stats
   private void makeExperiment(int N, int T)
   {
      Percolation perc = new Percolation(N);
      int[] pool = new int[N*N];
      for (int i = 0; i < pool.length; i++) pool[i] = i;
      StdRandom.shuffle(pool);

      int n = 0;
      do {
         int idx = pool[n++];
         perc.open(idx / N + 1, idx % N + 1);
      } while (!perc.percolates());
      
      results[T] = (double) n / pool.length;
   }

   public static void main(String[] args)    // test client (described below)
   {
      if (args.length != 2) {
         StdOut.println("Incorrect number of arguments!");
         return;
      }

      PercolationStats ps 
         = new PercolationStats(Integer.parseInt(args[0]),
          Integer.parseInt(args[1]));
      StdOut.printf("%-24s = %f\n%-24s = %f\n%-24s = %f , %f\n", 
         "mean", ps.mean(), "stddev", ps.stddev(), "95% confidence interval",
         ps.confidenceLo(), ps.confidenceHi());
      StdOut.println("Elapsed time: " + ps.timeElapsed);
   }
}

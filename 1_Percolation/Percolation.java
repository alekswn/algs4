import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;

public class Percolation {

   static private class DataStorage extends WeightedQuickUnionUF {
      static private final byte BLOCKED               = 0x0;
      static private final byte OPEN                  = 0x1;
      static private final byte FULL                  = OPEN | 0x2;
      static private final byte CONNECTED2DST         = OPEN | 0x4;
      static private final byte PERCOLATED            = FULL | CONNECTED2DST;
      private byte[] states;
      private boolean percolated;
      
      public DataStorage(int N) {
         super(N);
         states = new byte[N];
         percolated = false;
      }
      public void open(int n) {
            states[n] |= OPEN;
      }
      public boolean isOpen(int n) {
         return ((states[n] & OPEN) == OPEN);
      }
      public void full(int n) {
         states[find(n)] |= FULL;
      }
      public boolean isFull(int n) {
         return ((states[find(n)] & FULL) == FULL);
      }
      public void dst(int n) {
         states[find(n)] |= CONNECTED2DST;
      }
      public boolean isDst(int n) {
         return ((states[find(n)] & CONNECTED2DST) == CONNECTED2DST);
      }
      public boolean isPercolated(int n) {
         return ((states[find(n)] & PERCOLATED) == PERCOLATED);
      }
      public boolean isPercolated() {
         return percolated;
      }
      public void connect(int p, int q) {
         if (!isOpen(p) || !isOpen(q) || connected(p, q)) return;

         byte tst = states[find(p)];
         tst |= states[find(q)];
         if ((tst & PERCOLATED) == PERCOLATED) {
            if (!percolated) percolated = true; 
         }
         union(p, q);
         states[find(p)] = tst;
      }
   };
   
   private DataStorage dataStorage;
   private final int gridSize; 
   private final int numCells;
   // create N-by-N grid, with all sites blocked
   public Percolation(int N)
   {
      gridSize = N;
      numCells = N*N;
      if (gridSize <= 0)
         throw new IllegalArgumentException();
         
      dataStorage = new DataStorage(numCells+2);
      
      dataStorage.full(nTop());
   }
   // open site (row i, column j) if it is not open already
   public void open(int i, int j)
   {
      if (!validate(i, j)) throw new IndexOutOfBoundsException();
      final int p = ij2n(i, j);
      if (i == gridSize) dataStorage.dst(p);
      else dataStorage.open(p);

      int q = up(i, j);
      if (q >= 0) dataStorage.connect(p, q);
      q = down(i, j);
      if (q >= 0) dataStorage.connect(p, q);
      q = left(i, j);
      if (q >= 0) dataStorage.connect(p, q);
      q = right(i, j);
      if (q >= 0) dataStorage.connect(p, q);

   }
   public boolean isOpen(int i, int j)     // is site (row i, column j) open?
   {
      if (!validate(i, j)) throw new IndexOutOfBoundsException();
      return dataStorage.isOpen(ij2n(i, j));
   }
   public boolean isFull(int i, int j)     // is site (row i, column j) full?
   {
      if (!validate(i, j)) throw new IndexOutOfBoundsException();
      return dataStorage.isFull(ij2n(i, j));
   }
   public boolean percolates()             // does the system percolate?
   {
      return dataStorage.isPercolated();
   }
   
   public static void main(String[] args)  // test client (optional)
   {
         unitTests();
   }

   private String _toString() {
    StringBuilder result = new StringBuilder();
    String NEW_LINE = System.getProperty("line.separator");

    for (int i = 1; i <= gridSize; i++) {
      for (int j = 1; j <= gridSize; j++) {
         String ch = "#";
         if (dataStorage.isPercolated(ij2n(i, j))) ch = "+";
         if (isFull(i, j)) ch = "*";
         else if (isOpen(i, j)) ch = "O";
         result.append(' ' + ch);
      }
      result.append(NEW_LINE);
    }
   return result.toString();
   }

   private boolean validate(int i, int j) {
      if (i > 0 && j > 0 && i <= gridSize && j <= gridSize)
         return true;
      return false;
   }
   private int ij2n(int i, int j) // convert 2d indexes to UF array index
   {
      if (!validate(i, j)) return -1;
      return (i-1)*(gridSize) + j;
   }
   private int up(int i, int j)
   {
      if (!validate(i, j)) return -1;
      if (validate(i-1, j)) return ij2n(i-1, j);
      return nTop();
   }
   private int down(int i, int j)
   {
      if (!validate(i, j)) return -1;
      if (validate(i+1, j)) return ij2n(i+1, j);
      return nBot();
   }
   private int left(int i, int j)
   {
      if (!validate(i, j)) return -1;
      if (validate(i, j-1)) return ij2n(i, j-1);
      return -1;
   }
   private int right(int i, int j)
   {
      if (!validate(i, j)) return -1;
      if (validate(i, j+1)) return ij2n(i, j+1);
      return -1;
   }
   private int nTop()
   {
      return 0;
   }
   private int nBot()
   {
      return numCells + 1;
   }

   private static void unitTests()
   {
      Percolation percolation;
      final int size = 10;
      int test_counter = 0;
      StdOut.println("Percolation : Build-In test has begun");

      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     + " : Constructor with illegal argument ");
      try {
         percolation = new Percolation(0);
         StdOut.println("[failed] : no exeption");
      } catch (IllegalArgumentException e) {
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : Constructor with legal argument ");
      try {
         percolation = new Percolation(size);
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
         return;
      }
      StdOut.println(percolation._toString());


      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     + " : open() with illegal argument ");
      try {
         percolation.open(1, size+1);
         StdOut.println("[failed] : no exeption");
      } catch (IndexOutOfBoundsException e) {
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : open() with legal argument ");
      try {
         percolation.open(1, 1);
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.println(percolation._toString());

      StdOut.print("Percolation : Unit Test " + ++test_counter 
                        + " : isOpen() with illegal argument ");
      try {
         percolation.isOpen(1, size+1);
         StdOut.println("[failed] : no exeption");
      } catch (IndexOutOfBoundsException e) {
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }

      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : isOpen() with legal argument ");
      try {
         if (percolation.isOpen(1, 1) && !percolation.isOpen(size, size)) 
            StdOut.println("[passed]");
         else StdOut.println("[failed] : " + "Logical Error");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.println(percolation._toString());


      StdOut.print("Percolation : Unit Test " + ++test_counter 
                        + " : isFull() with illegal argument ");
      try {
         percolation.isFull(1, size+1);
         StdOut.println("[failed] : no exeption");
      } catch (IndexOutOfBoundsException e) {
         StdOut.println("[passed]");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }


      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : isFull() with legal argument ");
      percolation.open(2, 2);
      try {
         if (percolation.isFull(1, 1) 
            && !percolation.isFull(2, 2) && !percolation.isFull(size, size)) 
            StdOut.println("[passed]");
         else StdOut.println("[failed] : " + "Logical Error" 
                              + percolation.isFull(1, 1) + percolation.isFull(2, 2) 
                              + percolation.isFull(size, size));
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.println(percolation._toString());


      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : percolates() negative ");
      for (int i = 1; i <= size; ++i) percolation.open(i, i);
      try {
         if (!percolation.percolates()) 
            StdOut.println("[passed]");
         else StdOut.println("[failed] : " + "Logical Error");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.println(percolation._toString());

      
      StdOut.print("Percolation : Unit Test " + ++test_counter 
                     +" : percolates() positive ");
      for (int i = 1; i <= size; ++i) percolation.open(i, 1);
      try {
         if (percolation.percolates()) 
            StdOut.println("[passed]");
         else StdOut.println("[failed] : " + "Logical Error");
      } catch (Exception e) {
         StdOut.println("[failed] : " + e);
      }
      StdOut.println(percolation._toString());

   }
}

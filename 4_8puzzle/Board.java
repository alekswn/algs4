import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Queue;
import java.util.Iterator;

public class Board {
    private int[] barr;
    private int N;
    private int zeroPos;
    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks)
    {
        N = blocks.length;
        zeroPos = -1;
        assert (N >= 2 && N < 128);
        barr = new int[N*N];
        for (int i = 0; i < N; i++)
        {
            assert (N == blocks[i].length);
            System.arraycopy(blocks[i], 0, barr, i*N, N);
            if (zeroPos < 0)
            {
                for (int j = i*N; j < i*N + N; j++) 
                {
                    if (barr[j] == 0)
                    {
                        zeroPos = j;
                        break;
                    }
                }
            }
        }
        assert (zeroPos >= 0);
    }
    //copy constructor
    private Board(Board that)
    {
        barr = that.barr.clone();
        N = that.N;
        zeroPos = that.zeroPos;
    }

    // board dimension N
    public int dimension()
    {
        return N;
    }
    // number of blocks out of place
    public int hamming()
    {
        int out_count = 0;
        for (int i = 0; i < barr.length; i++) 
        {
            int v = barr[i] - 1;
            if (v != i && v >= 0) out_count++;
        }
        return out_count;
    }
    // sum of Manhattan distances between blocks and goal
    public int manhattan()
    {
        int res = 0;
        for (int i = 0; i < barr.length; i++) {
            int v = barr[i]-1;
            if (v == i || v < 0) continue;
            int dist = Math.abs(v / N - i / N) + Math.abs(v % N - i % N);
            if (dist > 0) res += dist;
        }
        return res;
    }
    // is this board the goal board?
    public boolean isGoal()
    {
        return (hamming() == 0);
    }
    // a board that is obtained by exchanging any pair of blocks
    public Board twin()
    {
        Board res = new Board(this);
        int i = 0;
        int j = 1;
        if (i == zeroPos || j == zeroPos)
        {
            i = N;
            j = N+1;
        }
        res.swap(i, j);
        return res;
    }
    // does this board equal y?
    public boolean equals(Object y)
    {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;
        if (that.N != N) return false;
        if (that.zeroPos != zeroPos) return false;
        for (int i = 0; i < barr.length; i++) 
            if (that.barr[i] != barr[i]) return false;
        return true;
    }
    // all neighboring boards
    public Iterable<Board> neighbors()
    {
        Queue<Board> res = new Queue<Board>();
        assert (zeroPos >= 0);
        if (zeroPos % N != 0)
        {
            Board left = new Board(this);
            left.swap(zeroPos, zeroPos - 1);
            res.enqueue(left);
        }
        if (zeroPos >= N)
        {
            Board up = new Board(this);
            up.swap(zeroPos, zeroPos - N);
            res.enqueue(up);
        }
        if (zeroPos % N < N - 1)
        {
            Board right = new Board(this);
            right.swap(zeroPos, zeroPos + 1);
            res.enqueue(right);
        }
        if (zeroPos < barr.length - N)
        {
            Board down = new Board(this);
            down.swap(zeroPos, zeroPos + N);
            res.enqueue(down);
        }
        
        return res;
    }
    // string representation of this board (in the output format specified below)
    public String toString()
    {
      StringBuilder result = new StringBuilder();
      result.append(N);
      for (int i = 0; i < barr.length; i++) {
        if (i % N == 0) result.append("\n");
        result.append(String.format("%2d ", barr[i]));
      }
      result.append("\n");
      return result.toString();
    }

    //swap two elements of barr
    private void swap(int i, int j)
    {
        int t;
        t = barr[j];
        barr[j] = barr[i];
        barr[i] = t;
        if (barr[i] == 0) zeroPos = i;
        if (barr[j] == 0) zeroPos = j;
    } 
    
    // unit tests (not graded)
    public static void main(String[] args)
    {
        int test_counter = 0;
        boolean test_failed = true;
        StdOut.println("Board : Build-In test has begun");

        StdOut.print("Board : Unit Test " + ++test_counter  +" : Constructor ");
        int[][] pattern_center = {
                            { 1, 2, 3 },
                            { 4, 0, 5 },
                            { 6, 7, 8 }
                                        };
        int[][] pattern_goal = {
                            { 1, 2, 3 },
                            { 4, 5, 6 },
                            { 7, 8, 0 }
                                        };
        Board board = new Board(pattern_center);
        Board goal  = new Board(pattern_goal);
        if (
            board.dimension() == 3
        &&  goal.dimension() == 3
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.println(board);
        StdOut.println(goal);
        StdOut.print("Board : Unit Test " + ++test_counter  +" : hamming() ");
        int[][] h1 = {
                    { 0, 2 },
                    { 3, 1 }
                };
        Board H1 = new Board(h1);
        if (
            board.hamming() == 4
        &&  goal.hamming() == 0
        &&  H1.hamming() == 1
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.print("Board : Unit Test " + ++test_counter  +" : manhattan() ");
        if (
            board.manhattan() == 6
        &&  goal.manhattan() == 0
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.print("Board : Unit Test " + ++test_counter  +" : isGoal() ");
        if (
            !board.isGoal()
        &&  goal.isGoal()
        &&  !H1.isGoal()
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.print("Board : Unit Test " + ++test_counter  +" : twin() ");
        Board twin = goal.twin();
        if (
            twin != null
        &&  !twin.isGoal()
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.println(twin);
        StdOut.print("Board : Unit Test " + ++test_counter  +" : equals() ");
        if (
            !board.equals(new Object())
        &&  !board.equals(goal)
//        &&  board.equals(board)
            )
            test_failed = false;
        if (test_failed) StdOut.println("[failed]");
        else { 
            StdOut.println("[passed]");
            test_failed = true;
        }
        StdOut.print("Board : Unit Test " + ++test_counter  +" : neighbors() ");
        StdOut.println("Neighbours:");
        Iterator<Board> it = board.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
        StdOut.println("Goal's neighbours:");
        it = goal.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
        int[][] p1 = {
                    { 0, 1 },
                    { 2, 3 }
                };
        Board d1 = new Board(p1);
        StdOut.println("d1 neighbours:");
        StdOut.println(d1.toString());
        it = d1.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
        int[][] p2 = {
                    { 1, 0 },
                    { 2, 3 }
                };
        Board d2 = new Board(p2);
        StdOut.println("d2 neighbours:");
        StdOut.println(d2.toString());
        it = d2.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
        int[][] p3 = {
                    { 1, 2 },
                    { 0, 3 }
                };
        Board d3 = new Board(p3);
        StdOut.println("d3 neighbours:");
        StdOut.println(d3.toString());
        it = d3.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
        int[][] p4 = {
                    { 1, 2 },
                    { 3, 0 }
                };
        Board d4 = new Board(p4);
        StdOut.println("d4 neighbours:");
        StdOut.println(d4.toString());
        it = d4.neighbors().iterator();
        while (it.hasNext()) StdOut.println(it.next());
    }
}

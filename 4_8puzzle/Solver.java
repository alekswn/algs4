import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import java.util.Iterator;

public class Solver {

    private class SearchNode implements Comparable<SearchNode> {
        private Board board;
        private int moves;
        private SearchNode prev;

        public SearchNode(Board board, int moves, SearchNode prev)
        {
            assert (board != null);
            assert (moves >= 0);
            this.board = board;
            this.moves = moves;
            this.prev = prev;
        }
        public Board getBoard() { return board; }
        public Iterable<SearchNode> succsessors()
        {
            assert (prev != null);
            Stack<SearchNode> res = new Stack<SearchNode>();
            Iterator<Board> it = board.neighbors().iterator();
            while (it.hasNext())
            {
                Board neighbor = it.next();
                if (prev != null && prev.board.equals(neighbor)) continue;
                SearchNode next = new SearchNode(neighbor, moves+1, this);
                res.push(next);
            }
            return res;
        }
        public int compareTo(SearchNode that) 
        {
            int manhattan = board.manhattan() - that.board.manhattan();
            int mvs = moves - that.moves;
            int priority = manhattan + mvs;
            if (priority != 0) return priority;
            int hamming = board.hamming() - that.board.hamming();
            return hamming + mvs;
        }
    }
    
    private MinPQ<SearchNode> game, twinGame;
    private Stack<Board> solution;
    
    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial)
    {
        if (initial == null) throw new NullPointerException();
        game = new MinPQ<SearchNode>();
        twinGame = new MinPQ<SearchNode>();
        solution = new Stack<Board>();

        SearchNode main_node = new SearchNode(initial, 0, null);
        SearchNode twin_node = new SearchNode(initial.twin(), 0, null);
        if (initial.isGoal()) solution.push(initial);
        else { 
            while (true) 
            {
                Iterator<SearchNode> it;
                it = main_node.succsessors().iterator();
                while (it.hasNext()) game.insert(it.next());
                it = twin_node.succsessors().iterator();
                while (it.hasNext()) twinGame.insert(it.next());

                main_node = game.delMin();
                twin_node = twinGame.delMin();

                if (main_node.getBoard().isGoal()) 
                {
                    solution.push(main_node.getBoard());
                    while (main_node.prev != null) {
                        solution.push(main_node.prev.getBoard());
                        main_node = main_node.prev;
                    }
                    break;
                }
                if (twin_node.getBoard().isGoal())
                {
                    solution = null;
                    break;
                }
            }
        }

    }
    // is the initial board solvable?
    public boolean isSolvable()
    {
        return (solution != null);
    }
    // min number of moves to solve initial board; -1 if unsolvable
    public int moves()
    {
        if (!isSolvable()) return -1;
        return solution.size() - 1;
    }
    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution()
    {
        return solution;
    }
    // solve a slider puzzle (given below)
    public static void main(String[] args)
    {
    // create initial board from file
    In in = new In(args[0]);
    int N = in.readInt();
    int[][] blocks = new int[N][N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            blocks[i][j] = in.readInt();
    Board initial = new Board(blocks);

    // solve the puzzle
    Solver solver = new Solver(initial);

    // print solution to standard output
    if (!solver.isSolvable())
        StdOut.println("No solution possible");
    else {
        StdOut.println("Minimum number of moves = " + solver.moves());
        for (Board board : solver.solution())
            StdOut.println(board);
    }
    }
}

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;
import java.awt.Color;
import java.util.Iterator;
import java.util.TreeSet;

public class PointSET {
   private TreeSet<Point2D> set;
   // construct an empty set of points 
   public PointSET()
   {
      set = new TreeSet<Point2D>();
   }
   // is the set empty?
   public boolean isEmpty() {
      return set.isEmpty();
   }
   // number of points in the set
   public int size()
   {
      return set.size();
   }
   // add the point to the set (if it is not already in the set)
   public void insert(Point2D p)
   {
      if (p == null) throw new NullPointerException();
      set.add(p);
   }
   // does the set contain point p?
   public boolean contains(Point2D p) 
   {
      if (p == null) throw new NullPointerException();
      return set.contains(p);
   }
   // draw all points to standard draw
   public void draw()
   {
      StdDraw.setPenRadius(0.01);
      Iterator<Point2D> it = set.iterator();
      while (it.hasNext()) 
      {
         Point2D p = it.next();
         StdDraw.point(p.x(), p.y());
      }
   }
   // all points that are inside the rectangle
   public Iterable<Point2D> range(RectHV rect)
   {
      if (rect == null) throw new NullPointerException();
      TreeSet<Point2D> res = new TreeSet<Point2D>();
      Iterator<Point2D> it = set.iterator();
      while (it.hasNext()) 
      {
         Point2D p = it.next();
         if (rect.contains(p)) res.add(p);
      }
      return res;
   }
   // a nearest neighbor in the set to point p; null if the set is empty
   public Point2D nearest(Point2D p) 
   {
      if (p == null) throw new NullPointerException();
      Iterator<Point2D> it = set.iterator();
      Point2D res = null;
      double minDistSq = Double.POSITIVE_INFINITY;
      while (it.hasNext())
      {
         Point2D t = it.next();
         double distSq = p.distanceSquaredTo(t); 
         if (distSq < minDistSq)
         {
            res = t;
            minDistSq = distSq;
         }
      }
      return res;
   }
   // unit testing of the methods (optional)
   public static void main(String[] args)
   {
      int test_counter = 0;
      boolean test_failed = true;
      StdOut.println("PointSET : Build-In test has begun");

      StdOut.print("PointSET : Unit Test " + ++test_counter  +" : Constructor ");
      PointSET points = new PointSET();
      if (
         points.isEmpty()
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("PointSET : Unit Test " + ++test_counter  +" : insert() ");
      Point2D p00 = new Point2D(0.0, 0.0);
      Point2D p01 = new Point2D(0.0, 1.0);
      Point2D p10 = new Point2D(1.0, 0.0);
      Point2D p11 = new Point2D(1.0, 1.0);
      Point2D p55 = new Point2D(0.5, 0.5);
      Point2D p15 = new Point2D(1.0, 0.5);
      points.insert(p00);
      points.insert(p01);
      points.insert(p10);
      points.insert(p11);
      points.insert(p55);
      points.insert(p15);
      points.draw();
      if (
            !points.isEmpty()
         && points.size() == 6
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("PointSET : Unit Test " + ++test_counter  +" : contains() ");
      if (
            points.contains(p00)
         && points.contains(p01)
         && points.contains(p10)
         && points.contains(p11)
         && points.contains(p55)
         && points.contains(p15)
         && !points.contains(new Point2D(0.1, 0.1))
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("PointSET : Unit Test " + ++test_counter  +" : nearest() ");
      Point2D p44 = new Point2D(0.4, 0.4);
      StdDraw.setPenColor(Color.GREEN);
      StdDraw.point(p44.x(), p44.y());
      Point2D np = points.nearest(p44);
      if (
            np.equals(p55)
         )
         test_failed = false;
      StdDraw.setPenColor(Color.RED);
      StdDraw.point(np.x(), np.y());

      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("PointSET : Unit Test " + ++test_counter  +" : range() ");
      RectHV rect = new RectHV(0.2, 0.2, 1.0, 0.6);
      StdDraw.setPenColor(Color.BLUE);
      StdDraw.setPenRadius(0.001);
      StdDraw.rectangle((rect.xmin()+rect.xmax())/2.0, 
                        (rect.ymin()+rect.ymax())/2.0,
                        rect.width()/2.0,
                        rect.height()/2.0);
      Iterator<Point2D> it = points.range(rect).iterator();
      Point2D p1 = it.next();
      Point2D p2 = it.next();
      if (
            !it.hasNext()
         && (p1.equals(p55) || p1.equals(p15))
         && (p2.equals(p55) || p2.equals(p15))
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
   }
}

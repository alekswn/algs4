import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;
import java.awt.Color;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


public class KdTree {   
   private static class Node {
      private Point2D p;      // the point
      private Node lb;        // the left/bottom subtree
      private Node rt;        // the right/top subtree
      private RectHV rect;
   
      public Node(Point2D p, RectHV rect) 
      { 
         this.p = p;
         this.rect = rect;
      }
   }

   private static class PointDist
   {
      private Point2D p;
      private double d;
      
      public PointDist(Point2D p, double d)
      {
         this.p = p;
         this.d = d;
      }
   }

   private Node root;
   private int sz;

   // construct an empty set of points 
   public KdTree()
   {
      root = null;
      sz = 0;
   }
   // is the set empty?
   public boolean isEmpty() {
      return (root == null);
   }
   // number of points in the set
   public int size()
   {
      return sz;
   }
   // add the point to the set (if it is not already in the set)
   public void insert(Point2D p)
   {
      if (p == null) throw new NullPointerException();
      root = put(root, p, 0, null, false);
   }
   // does the set contain point p?
   public boolean contains(Point2D p) 
   {
      if (p == null) throw new NullPointerException();
      return (get(root, p, 0) != null);
   }
   // draw all rectangles to standard draw
   public void draw()
   {
      draw(root);
   }
   // all points that are inside the rectangle
   public Iterable<Point2D> range(RectHV rect)
   {
      if (rect == null) throw new NullPointerException();
      TreeSet<Point2D> res = new TreeSet<Point2D>();
      range(root, rect, 0, res);
      return res;
   }
   // a nearest neighbor in the set to point p; null if the set is empty
   public Point2D nearest(Point2D p) 
   {
      if (p == null) throw new NullPointerException();
      return nearest(root, p, new PointDist(null, Double.POSITIVE_INFINITY), true).p;
   }

   private PointDist nearest(Node node, Point2D p, PointDist res, boolean isEven)
   {
      if (node == null) return res;
      if (node.rect.distanceSquaredTo(p) >= res.d) return res;
      double distSq = node.p.distanceSquaredTo(p);
      if (distSq < res.d) res = new PointDist(node.p, distSq);
      if ((isEven && p.x() < node.p.x()) || (!isEven && p.y() < node.p.y()))
      {
         res = nearest(node.lb, p, res, !isEven);
         res = nearest(node.rt, p, res, !isEven);
      } else {
         res = nearest(node.rt, p, res, !isEven);
         res = nearest(node.lb, p, res, !isEven);
      }
      return res;
   }
   private Node put(Node node, Point2D p, int level, Node parent, boolean isRight)
   {
      assert (p != null);
      assert (level >= 0);
      boolean isEven = (level++ % 2 == 0);
      if (node == null)
      {
         sz++;
         double xmin, ymin, xmax, ymax;
         if (parent == null)
         {
            xmin = 0.0;
            ymin = 0.0;
            xmax = 1.0;
            ymax = 1.0;
         } else {
            if (isEven) //parent is odd
            {
               xmin = parent.rect.xmin();
               xmax = parent.rect.xmax();
               if (isRight)
               {
                  ymin = parent.p.y();
                  ymax = parent.rect.ymax();
               } else {
                  ymin = parent.rect.ymin();
                  ymax = parent.p.y();
               }
            } else { //parent is even
               ymin = parent.rect.ymin();
               ymax = parent.rect.ymax();
               if (isRight)
               {
                  xmin = parent.p.x();
                  xmax = parent.rect.xmax();
               } else {
                  xmin = parent.rect.xmin();
                  xmax = parent.p.x();
               }
            }
         }
         return new Node(p, new RectHV(xmin, ymin, xmax, ymax));
      }
      if (p.equals(node.p)) return node;
      if ((isEven && p.x() >= node.p.x()) || (!isEven && p.y() >= node.p.y())) 
         node.rt = put(node.rt, p, level, node, true);
      else node.lb = put(node.lb, p, level, node, false);
      return node;
   }
   private Node get(Node node, Point2D p, int level)
   {
      assert (p != null);
      assert (level >= 0);
      if (node == null) return null;
      if (p.equals(node.p)) return node;
      boolean isEven = (level++ % 2 == 0);
      if ((isEven && p.x() >= node.p.x()) || (!isEven && p.y() >= node.p.y())) 
         return get(node.rt, p, level);
      else return get(node.lb, p, level);
   }
   private void draw(Node node)
   { 
      if (node == null) return;
      StdDraw.setPenRadius(0.001);
      StdDraw.setPenColor(Color.RED);
      StdDraw.line(node.rect.xmin(), node.rect.ymin(), 
                     node.rect.xmin(), node.rect.ymax());
      StdDraw.line(node.rect.xmax(), node.rect.ymin(), 
                     node.rect.xmax(), node.rect.ymax());
      StdDraw.setPenColor(Color.BLUE);
      StdDraw.line(node.rect.xmin(), node.rect.ymin(), 
                     node.rect.xmax(), node.rect.ymin());
      StdDraw.line(node.rect.xmin(), node.rect.ymax(), 
                     node.rect.xmax(), node.rect.ymax());

      draw(node.lb);
      draw(node.rt);
      StdDraw.setPenColor(Color.BLACK);
      StdDraw.setPenRadius(0.01);
      StdDraw.point(node.p.x(), node.p.y());
   }
   private void drawArr(Node node, double pen)
   {
      if (node == null) return;
      if (node.lb != null)
      {
         StdDraw.setPenRadius(pen);
         StdDraw.setPenColor(Color.GREEN);
         StdDraw.line(node.p.x(), node.p.y(), node.lb.p.x(), node.lb.p.y());
         drawArr(node.lb, pen / 2.0);
      }
      if (node.rt != null)
      {
         StdDraw.setPenRadius(pen);
         StdDraw.setPenColor(Color.RED);
         StdDraw.line(node.p.x(), node.p.y(), node.rt.p.x(), node.rt.p.y());
         drawArr(node.rt, pen / 2.0);
      }
      StdDraw.setPenRadius(pen);
      StdDraw.setPenColor(Color.BLUE); 
      StdDraw.point(node.p.x(), node.p.y());
   }

   private Set<Point2D> range(Node node, RectHV rect, int level, Set<Point2D> res)
   {
      assert (res != null);
      assert (rect != null);
      assert (level >= 0);
      if (node == null) return res;
      double x = node.p.x();
      double y = node.p.y(); 
      if (x <= rect.xmax() && y <= rect.ymax() 
         && x >= rect.xmin() && y >= rect.ymin()) res.add(node.p);
      if (level++ % 2 == 0)
      {
         if (rect.xmin() <  x) range(node.lb, rect, level, res);
         if (rect.xmax() >= x) range(node.rt, rect, level, res);
      } else {
         if (rect.ymin() <  y) range(node.lb, rect, level, res);
         if (rect.ymax() >= y) range(node.rt, rect, level, res);
      }
      return res;
   }
   // unit testing of the methods (optional)
   public static void main(String[] args)
   {
      int test_counter = 0;
      boolean test_failed = true;
      StdOut.println("KdTree : Build-In test has begun");

      StdOut.print("KdTree : Unit Test " + ++test_counter  +" : Constructor ");
      KdTree points = new KdTree();
      if (
         points.isEmpty()
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("KdTree : Unit Test " + ++test_counter  +" : insert() ");
      Point2D p00 = new Point2D(0.0, 0.0);
      Point2D p01 = new Point2D(0.0, 1.0);
      Point2D p10 = new Point2D(1.0, 0.0);
      Point2D p11 = new Point2D(1.0, 1.0);
      Point2D p55 = new Point2D(0.5, 0.5);
      Point2D p15 = new Point2D(1.0, 0.5);
      Point2D p55_40 = new Point2D(0.55, 0.4);
      points.insert(p55);
      points.insert(p00);
      points.insert(p01);
      points.insert(p10);
      points.insert(p11);
      points.insert(p15);
      points.insert(p55_40);
//      points.drawArr(points.root, 0.1);
      points.draw();
      if (
            !points.isEmpty()
         && points.size() == 7
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("KdTree : Unit Test " + ++test_counter  +" : contains() ");
      if (
            points.contains(p00)
         && points.contains(p01)
         && points.contains(p10)
         && points.contains(p11)
         && points.contains(p55)
         && points.contains(p15)
         && points.contains(p55_40)
         && !points.contains(new Point2D(0.1, 0.1))
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("KdTree : Unit Test " + ++test_counter  +" : range() ");
      RectHV rect = new RectHV(0.51, 0.2, 1.0, 0.6);
//      StdDraw.rectangle((rect.xmin()+rect.xmax())/2.0, 
//                        (rect.ymin()+rect.ymax())/2.0,
//                        rect.width()/2.0,
//                        rect.height()/2.0);
      Iterator<Point2D> it = points.range(rect).iterator();
      Point2D p1 = it.next();
      Point2D p2 = it.next();
      if (
            !it.hasNext()
         && (p1.equals(p55_40) || p1.equals(p15))
         && (p2.equals(p55_40) || p2.equals(p15))
         )
         test_failed = false;
      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
      StdOut.print("KdTree : Unit Test " + ++test_counter  +" : nearest() ");
      Point2D p45_40 = new Point2D(0.45, 0.4);
//      StdDraw.setPenRadius(0.01);
//      StdDraw.setPenColor(Color.GREEN);
//      StdDraw.point(p45_40.x(),p45_40.y());
      Point2D np = points.nearest(p45_40);
      if (
            np != null
         && np.equals(p55_40)
         )
         test_failed = false;
//      StdDraw.point(np.x(),np.y());

      if (test_failed) StdOut.println("[failed]");
      else { 
         StdOut.println("[passed]");
         test_failed = true;
      }
   }
}

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {
    private final WordNet wn;
   public Outcast(WordNet wordnet)         // constructor takes a WordNet 
   {
       wn = wordnet;
   }
   // given an array of WordNet nouns, return an outcast
   public String outcast(String[] nouns)
    {
        int maxDist = -1;
        int out = -1;
        for (int i = 0; i < nouns.length; i++) {
            int dist = 0;
            for (int j = 0; j < nouns.length; j++) {
                if (i == j) continue;
                dist += wn.distance(nouns[i], nouns[j]);
//                StdOut.printf("%15s - %15s :%2d \"%s\"\n", nouns[i], nouns[j], 
//                   wn.distance(nouns[i], nouns[j]), wn.sap(nouns[i], nouns[j]));
            }
            if (maxDist < dist) {
                maxDist = dist;
                out = i;
            }
//            StdOut.printf("%s:\t %d\n\n", nouns[i], dist);
        }
        return nouns[out];
    }
   
   public static void main(String[] args) {
    WordNet wordnet = new WordNet(args[0], args[1]);
    Outcast outcast = new Outcast(wordnet);
    for (int t = 2; t < args.length; t++) {
        In in = new In(args[t]);
        String[] nouns = in.readAllStrings();
        StdOut.println(args[t] + ": " + outcast.outcast(nouns));
    }
}
}

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeMap;

public class SAP {
    private final ComplexBreadFirstSerch cbfs;
    private ArrayList<Iterable<Integer>> mCachedSources;
    private SearchResult mCachedSearchResults;

   // constructor takes a digraph (not necessarily a DAG)
   public SAP(Digraph G) {
       cbfs = new ComplexBreadFirstSerch(G);
       }

   // length of shortest ancestral path between v and w; -1 if no such path
   public int length(int v, int w) //TODO performance best and worst cases
   {
       ArrayList<Integer> vCol = new ArrayList<Integer>();
       ArrayList<Integer> wCol = new ArrayList<Integer>();
       vCol.add(v);
       wCol.add(w);
       return length(vCol, wCol);
   }

   // a common ancestor of v and w that participates in a shortest ancestral path;
   //-1 if no such path
   public int ancestor(int v, int w) //TODO performance best and worst cases
   {
       LinkedList<Integer> vCol = new LinkedList<Integer>();
       LinkedList<Integer> wCol = new LinkedList<Integer>();
       vCol.add(v);
       wCol.add(w);
       return ancestor(vCol, wCol);
    }

   // length of shortest ancestral path between any vertex in v and any vertex in w;
   //-1 if no such path
   public int length(Iterable<Integer> v, Iterable<Integer> w)
   {
       if (v == null || w == null) throw new java.lang.NullPointerException();
       return runComplexBfs(v, w).sumOfLengths;
   }

   // a common ancestor that participates in shortest ancestral path;
   //-1 if no such path
   public int ancestor(Iterable<Integer> v, Iterable<Integer> w)
   {
       if (v == null || w == null) throw new java.lang.NullPointerException();
       return runComplexBfs(v, w).ancestor;
   }

   //a helper function
   private SearchResult runComplexBfs(Iterable<Integer> v, Iterable<Integer> w)
    {
       if (v == null || w == null) throw new java.lang.NullPointerException();
       ArrayList<Iterable<Integer>> sources = new ArrayList<Iterable<Integer>>(2);
       sources.add(v);
       sources.add(w);
       
       if (mCachedSources == null || !mCachedSources.equals(sources)) {
//            StdOut.println("DEBUG: updating cache");
            mCachedSources = new ArrayList<Iterable<Integer>>(2);
            LinkedList<Integer> vCopy = new LinkedList<Integer>();
            for (int i : v) vCopy.add(i);
            LinkedList<Integer> wCopy = new LinkedList<Integer>();
            for (int i : w) wCopy.add(i);
            mCachedSources.add(vCopy);
            mCachedSources.add(wCopy);
            mCachedSearchResults = cbfs.runUntilFirstAncestor(vCopy, wCopy);
        }
       return mCachedSearchResults;
//        return cbfs.libBfs(sources);
    }
    
    private class ComplexBreadFirstSerch {
        private static final int NOT_FOUND = -1;
        private final Digraph graph;
        public ComplexBreadFirstSerch(Digraph G)
        {
            graph = new Digraph(G);
        }
        public SearchResult runUntilFirstAncestor(LinkedList<Integer> vList,
                                                  LinkedList<Integer> wList) {
            //initialize BFS queues
            LinkedList<Integer> qV = new LinkedList<Integer>();
            LinkedList<Integer> qW = new LinkedList<Integer>();
            TreeMap<Integer, Integer> distToV = new TreeMap<Integer, Integer>();
            TreeMap<Integer, Integer> distToW = new TreeMap<Integer, Integer>();
            
            for (int v : vList) {
                qV.push(v);
                distToV.put(v, 0);
            }
            for (int w : wList) {
                qW.push(w);
                distToW.put(w, 0);
            }
            
            //run BFS step-by-step simalteniously for both sources
            int level = 1;
            int ancestor = NOT_FOUND;
            int length = Integer.MAX_VALUE;
            while (length >= level && (!qV.isEmpty() || !qW.isEmpty())) {
                for (int i = qV.size(); i > 0; i--) {
                    int v = qV.pollLast();
                    //Cross check if ancestor was found
                    if (distToW.containsKey(v)) {
                        int lng = distToW.get(v) + distToV.get(v);
                        if (length > lng) {
                            length = lng;
                            ancestor = v;
                        }
                    }
                   for (int next : graph.adj(v)) {
                        //Get appropriate distance entry, do nothing if this vertex 
                        //was visited from this source already
                        if (distToV.containsKey(next)) continue;
                        distToV.put(next, level);
                        qV.push(next);
                    }
                }
                
                for (int i = qW.size(); i > 0; i--) {
                    int w = qW.pollLast();
                    //Cross check if ancestor was found
                    if (distToV.containsKey(w)) {
                        int lng = distToW.get(w) + distToV.get(w);
                        if (length > lng) {
                            length = lng;
                            ancestor = w;
                        }
                    }
                    for (int next : graph.adj(w)) {
                        //Get appropriate distance entry, do nothing if this vertex 
                        //was visited from this source already
                        if (distToW.containsKey(next)) continue;
                        distToW.put(next, level);
                        qW.push(next);
                    }
                }
                level++;
            }
            if (ancestor == NOT_FOUND) length = NOT_FOUND;
            return new SearchResult(length, ancestor);
        }
        public SearchResult libBfs(Iterable<Iterable<Integer>> sources) 
                                                // worst case ~ V+E; best case ~ V
        {
            LinkedList<BreadthFirstDirectedPaths> bfsList =
                                    new LinkedList<BreadthFirstDirectedPaths>();
            int ancestor = NOT_FOUND;
            int min_length = Integer.MAX_VALUE;
            for (Iterable<Integer> s : sources)
                bfsList.add(new BreadthFirstDirectedPaths(graph, s));
            for (int v = 0; v < graph.V(); v++) 
            {
                int length = 0;
                for (BreadthFirstDirectedPaths bfs : bfsList)
                {
                    if (!bfs.hasPathTo(v))
                    {
                        length = NOT_FOUND;
                        break;
                    }
                    length += bfs.distTo(v); 
                }
                if (length >= 0 && length < min_length)
                {
                    min_length = length;
                    ancestor = v;
                }
            }
            if (ancestor == NOT_FOUND) min_length = NOT_FOUND;
            return new SearchResult(min_length, ancestor);
        }
    }
    
    private class SearchResult {
        private final int sumOfLengths;
        private final int ancestor;
        public SearchResult(int lng, int anc) {
            sumOfLengths = lng;
            ancestor = anc;
        }
    }

   // do unit testing of this class
   public static void main(String[] args) {
    In in = new In(args[0]);
    Digraph G = new Digraph(in);
    SAP sap = new SAP(G);
    while (!StdIn.isEmpty()) {
        int v = StdIn.readInt();
        int w = StdIn.readInt();
        int length   = sap.length(v, w);
        int ancestor = sap.ancestor(v, w);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
    }
}

}

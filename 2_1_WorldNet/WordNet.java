import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Topological;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;

public class WordNet {
   private final HashMap<String, LinkedList<Integer>> synmap;
   private final ArrayList<String> synlist;
   private final SAP hypernyms;

   // constructor takes the name of the two input files
   public WordNet(String synsetsFn, String hypernymsFn) { // ~ n
      if (synsetsFn == null || hypernymsFn == null)
         throw new java.lang.NullPointerException();

      In synsetsIn = new In(synsetsFn);
      In hypernymsIn = new In(hypernymsFn);
      HashMap<String, LinkedList<Integer>> hashmap 
         = new HashMap<String, LinkedList<Integer>>();
      ArrayList<String> arraylist = new ArrayList<String>();
      int maxID = -1;
      String line;
      while ((line = synsetsIn.readLine()) != null) {
         String[] columns = line.split(",");
         String[] syns = columns[1].split(" ");
         int id = Integer.parseInt(columns[0]);
         if (maxID < id) maxID = id;
         for (String syn : syns) {
            LinkedList<Integer> val = hashmap.get(syn);
            if (val == null) {
               val = new LinkedList<Integer>();
               hashmap.put(syn, val);
            }
            val.push(id);
         }
         arraylist.add(id, columns[1]);
      }
      synmap = hashmap;
      synlist = arraylist;

      Digraph graph = new Digraph(maxID + 1);
      while ((line = hypernymsIn.readLine()) != null) {
         String[] columns = line.split(",");
         int v = Integer.parseInt(columns[0]);
         for (int i = 1; i < columns.length; i++)
            graph.addEdge(v, Integer.parseInt(columns[i]));
      }

      int rootsNum = 0;
      for (int i = 0; i < graph.V(); i++) 
         if (graph.outdegree(i) == 0) rootsNum++;
      if (rootsNum > 1)
         throw new java.lang.IllegalArgumentException("Hypersyms must correspond"
                                                            + "have one root only");
      if (!(new Topological(graph)).hasOrder())
         throw new java.lang.IllegalArgumentException("Hypersyms must correspond"
                                                                     + "to a DAG");

      hypernyms = new SAP(graph);
   }

   // returns all WordNet nouns
   public Iterable<String> nouns() {
      return synmap.keySet();
    }

   // is the word a WordNet noun?
   public boolean isNoun(String word) {
       if (word == null) throw new java.lang.NullPointerException();
       return synmap.containsKey(word);
    }

   // distance between nounA and nounB (defined below)
   public int distance(String nounA, String nounB) {
       if (!isNoun(nounA) || !isNoun(nounB)) 
         throw new java.lang.IllegalArgumentException();

      return hypernyms.length(synmap.get(nounA), synmap.get(nounB));
    }

   // a synset (second field of synsets.txt) that is the common ancestor of nounA 
   // and nounB in a shortest ancestral path (defined below)
   public String sap(String nounA, String nounB) {
       if (!isNoun(nounA) || !isNoun(nounB)) 
         throw new java.lang.IllegalArgumentException();

      return synlist.get(hypernyms.ancestor(synmap.get(nounA), synmap.get(nounB)));
   }   

   // do unit testing of this class
   public static void main(String[] args) {
       WordNet wordnet = new WordNet(args[0], args[1]);
//       StdOut.printf("Nouns:\n");
//       for ( String s : wordnet.nouns() ) StdOut.printf("\t%s\n",s);
       String nounA = StdIn.readString();
       String nounB = StdIn.readString();
       String ancensor =  wordnet.sap(nounA, nounB);
       StdOut.printf("%s %d\n", ancensor, wordnet.distance(nounA, nounB));
      }
}

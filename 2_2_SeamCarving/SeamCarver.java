import edu.princeton.cs.algs4.Picture;

import java.awt.Color;
import java.util.ArrayList;

public class SeamCarver {
    private static final double BORDER_ENERGY = 1000; 
    private static final int SOURCE_INDEX = -1;
    
    private Picture picture;
    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null) throw new java.lang.NullPointerException();
        this.picture = new Picture(picture);
    }
    // current picture
    public Picture picture() {
        return new Picture(picture);
    }
    // width of current picture
    public     int width() {
        return picture.width();
    }
    // height of current picture
    public     int height() {
        return picture.height();
    }
    // energy of pixel at column x and row y
    public  double energy(int x, int y) {
        if (x < 0 || y < 0 || x >= width() || y >= height())
            throw new java.lang.IndexOutOfBoundsException();
        return dualGradientEnergy(x, y);
    }
    // sequence of indices for horizontal seam
    public   int[] findHorizontalSeam() {
        ArrayList<SeamLayer> layers = new ArrayList<SeamLayer>(width());
        
        //Fill in the first layer
        SeamLayer currentLayer = new SeamLayer(height());
        for (int j = 0; j < height(); j++) 
            currentLayer.add(j, SOURCE_INDEX, energy(0,j));

        //Relax all interlayer edges in loop
        for (int i = 1; i < width(); i++) {
            SeamLayer nextLayer = new SeamLayer(height());
            for (int j = 0; j < height(); j++) {
                double e = energy(i,j);
                nextLayer.add(j, j-1, e + currentLayer.getDistTo(j-1));
                nextLayer.relax(j, j, e + currentLayer.getDistTo(j));
                nextLayer.relax(j, j+1, e + currentLayer.getDistTo(j+1));
            }
            layers.add(currentLayer);
            currentLayer = nextLayer;
        }

        //Traverse minimal path
        int minPathEdge = currentLayer.getMinPathEdge();
        int[] result = new int[width()];
        result[result.length - 1] = minPathEdge;
        minPathEdge = currentLayer.edgeTo.get(minPathEdge);
        for (int i = result.length - 2; i >= 0; i--) {
            result[i] = minPathEdge;
            minPathEdge = layers.get(i).edgeTo.get(minPathEdge);
        }
        assert(minPathEdge == SOURCE_INDEX);
        return result;
    }
    // sequence of indices for vertical seam
    public   int[] findVerticalSeam() {
        //TODO optimize 
/*
        picture = transpose();
        int[] result = findHorizontalSeam();
        picture = transpose();
*/
        ArrayList<SeamLayer> layers = new ArrayList<SeamLayer>(height());
        
        //Fill in the first layer
        SeamLayer currentLayer = new SeamLayer(width());
        for (int i = 0; i < width(); i++) 
            currentLayer.add(i, SOURCE_INDEX, energy(i,0));

        //Relax all interlayer edges in loop
        for (int j = 1; j < height(); j++) {
            SeamLayer nextLayer = new SeamLayer(width());
            for (int i = 0; i < width(); i++) {
                double e = energy(i,j);
                nextLayer.add(i, i-1, e + currentLayer.getDistTo(i-1));
                nextLayer.relax(i, i, e + currentLayer.getDistTo(i));
                nextLayer.relax(i, i+1, e + currentLayer.getDistTo(i+1));
            }
            layers.add(currentLayer);
            currentLayer = nextLayer;
        }

        //Traverse minimal path
        int minPathEdge = currentLayer.getMinPathEdge();
        int[] result = new int[height()];
        result[result.length - 1] = minPathEdge;
        minPathEdge = currentLayer.edgeTo.get(minPathEdge);
        for (int i = result.length - 2; i >= 0; i--) {
            result[i] = minPathEdge;
            minPathEdge = layers.get(i).edgeTo.get(minPathEdge);
        }
        assert(minPathEdge == SOURCE_INDEX);
        return result;
    }
    // remove horizontal seam from current picture
    public    void removeHorizontalSeam(int[] seam) {
        if (seam == null) throw new java.lang.NullPointerException();
        if (seam.length != width()) 
            throw new java.lang.IllegalArgumentException("Incorrect seam length");
        if (height() <= 1)
            throw new java.lang.IllegalArgumentException("Insufficient width");
        
        //TODO optimize
        Picture newPic = new Picture(width(), height() - 1);
        for (int i = 0; i < width(); i++) {
            if (seam[i] < 0 || seam[i] >= height())
                throw new java.lang.IllegalArgumentException("Invalid seam");
            if (i > 0) {
                int d = seam[i] - seam[i - 1];
                if (d > 1 || d < -1) 
                    throw new java.lang.IllegalArgumentException(
                            "Seam is not connected properlly");
            }
            for (int j = 0; j < height(); j++) {
                if (j == seam[i]) continue;
                if (j > seam[i]) newPic.set( i, j - 1, picture.get(i,j) );
                else newPic.set( i, j, picture.get(i,j) );
            }
        }
        picture = newPic;
    }
    // remove vertical seam from current picture
    public    void removeVerticalSeam(int[] seam) {
        if (seam == null) throw new java.lang.NullPointerException();
        if (seam.length != height()) 
            throw new java.lang.IllegalArgumentException("Incorrect seam length");
        if (width() <= 1)
            throw new java.lang.IllegalArgumentException("Insufficient height");
        
        //TODO optimize
        Picture newPic = new Picture(width() - 1, height());
        for (int i = 0; i < width(); i++) {
            for (int j = 0; j < height(); j++) {
                if (seam[j] < 0 || seam[j] >= width())
                        throw new java.lang.IllegalArgumentException("Invalid seam");
                if (i == seam[j]) {
                    if (j > 0) {
                        int d = seam[j] - seam[j - 1];
                        if (d > 1 || d < -1) 
                            throw new java.lang.IllegalArgumentException(
                                "Seam is not connecte properlly");
                    }
                    continue;
                }
                if (i > seam[j]) newPic.set( i - 1, j, picture.get(i,j) );
                else newPic.set( i, j, picture.get(i,j) );
            }
        }
        picture = newPic; 
    }
    
    private double dualGradientEnergy(int x, int y) {
        if (x == 0 || y == 0 || x == (width() - 1) || y == (height() - 1))
            return BORDER_ENERGY;

        final Color pX = picture.get(x - 1, y);
        final Color sX = picture.get(x + 1, y);
        final Color pY = picture.get(x, y - 1);
        final Color sY = picture.get(x, y + 1);

        final int RpX  = pX.getRed();
        final int GpX  = pX.getGreen();
        final int BpX  = pX.getBlue();
        final int RsX  = sX.getRed();
        final int GsX  = sX.getGreen();
        final int BsX  = sX.getBlue();
        final int RpY  = pY.getRed();
        final int GpY  = pY.getGreen();
        final int BpY  = pY.getBlue();
        final int RsY  = sY.getRed();
        final int GsY  = sY.getGreen();
        final int BsY  = sY.getBlue();

        final int dRx  = RpX - RsX;
        final int dGx  = GpX - GsX;
        final int dBx  = BpX - BsX;
        final int dRy  = RpY - RsY;
        final int dGy  = GpY - GsY;
        final int dBy  = BpY - BsY;

        final int Dx2  = dRx*dRx + dGx*dGx + dBx*dBx;
        final int Dy2  = dRy*dRy + dGy*dGy + dBy*dBy;

        return Math.sqrt(Dx2 + Dy2);
    }
    
    private Picture transpose() {
        Picture newPic = new Picture(height(), width());
        for (int i = 0; i < width(); i++)
            for (int j = 0; j < height(); j++)
                newPic.set(j,i,picture.get(i,j));
        return newPic;
    }
    
    private class SeamLayer {
            private final int size;
            private final ArrayList<Integer> edgeTo;
            private final ArrayList<Double>  distTo;
            public void add(int i, int g, double d) {
                assert( i == distTo.size());
                edgeTo.add(g);
                distTo.add(d);
            }
            public void relax(int i, int g, double d) {
                assert( i == distTo.size() - 1);
                Double cd = distTo.get(i);
                if (d < cd) {
                    edgeTo.set(i,g);
                    distTo.set(i,d);
                }
            }
            public int size() {
                return size;
            }
            public double getDistTo(int i) {
                assert( i >= -1 && i <= size );
                if (i < 0 || i >= distTo.size()) return Double.POSITIVE_INFINITY;
                return distTo.get(i);
            }
            public int getMinPathEdge() {
                int minEdge = SOURCE_INDEX;
                double minDist = Double.POSITIVE_INFINITY;
                for (int i = 0; i < distTo.size(); i++) {
                    double dist = distTo.get(i);
                    if (dist < minDist) {
                        minDist = dist;
                        minEdge = i;
                    }
                }
                return minEdge;
            }
            public SeamLayer(int sz) {
                assert( sz > 0 );
                size = sz;
                edgeTo = new ArrayList<Integer>(sz);
                distTo = new ArrayList<Double>(sz);
            }
        }
}


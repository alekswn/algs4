import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {
    // alphabet size of extended ASCII
    private static final int R = 256;

    // Do not instantiate.
//    private MoveToFront() { }
    
    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        // read the input
        String s = BinaryStdIn.readString();
        char[] input = s.toCharArray();
        
        //initialize array
        int[] arr = new int[R];
        for (int i = 0; i < R; i++) arr[i] = i;

        for ( char ch : input ) {
            int i = 0;
            while (arr[i] != ch) i++;
            BinaryStdOut.write((char)i);
            int t = arr[i];
            System.arraycopy( arr, 0, arr, 1, i );
            arr[0] = t;
        }
        
        // close output stream
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        // read the input
        String s = BinaryStdIn.readString();
        char[] input = s.toCharArray();
        
        //initialize array
        int[] arr = new int[R];
        for (int i = 0; i < R; i++) arr[i] = i;

        for ( int i : input ) {
            BinaryStdOut.write((char)arr[i]);
            int t = arr[i];
            System.arraycopy( arr, 0, arr, 1, i );
            arr[0] = t;
        }
        
        // close output stream
        BinaryStdOut.close();    }

    // if args[0] is '-', apply move-to-front encoding
    // if args[0] is '+', apply move-to-front decoding
    public static void main(String[] args) {
        if      (args[0].equals("-")) encode();
        else if (args[0].equals("+")) decode();
        else throw new IllegalArgumentException("Illegal command line argument");
    }
}

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.StdOut;


public class BurrowsWheeler {
    // alphabet size of extended ASCII
    private static final int R = 256;

    // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
    public static void encode() {
        // read the input
        String s = BinaryStdIn.readString();
        int lng = s.length();
        char[] output = new char[lng];
        CircularSuffixArray csa = new CircularSuffixArray(s);
        int firstIdx = -1;
        for (int i = 0; i < lng; i++) {
            int idx  = csa.index( i );
            if (idx == 0) {
                firstIdx = i;
                idx = lng;
            }
            output[i] = s.charAt(--idx);
        }
        assert (firstIdx >= 0 && firstIdx < lng);
        BinaryStdOut.write(firstIdx);
        for (char ch : output) BinaryStdOut.write(ch);
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
    public static void decode() {
        int firstIdx = BinaryStdIn.readInt();
        String input = BinaryStdIn.readString();
        int lng = input.length();
        
        class Bucket {
            class Flower {
                private int idx;
                private Flower next;
                public Flower( int idx ) {
                    this.idx = idx;
                }
            }
            private Flower first;
            private Flower last;
            
            public Bucket(int idx) {
                last = new Flower(idx);
                first = last;
            }
            public void push (int idx) {
                assert( first != null && last != null);
                last.next = new Flower(idx);
                last = last.next;
            }
            public int poll () {
                assert (first != null && last != null);
                int res = first.idx;
                first = first.next;
                return res;
            }
            public boolean isEmpty() {
                return first == null;
            }
        }

        //Bucket sort
        Bucket[] buckets = new Bucket[R];
        for (int i = 0; i < lng; i++) {
            char ch = input.charAt(i);
            if (buckets[ch] == null) buckets[ch] = new Bucket( i );
            else buckets[ch].push( i );
        }
        
        //Create next[]
        int[] next = new int[lng];
        char ch = 0;
        for (int i = 0; i < lng; i++) {
            while (buckets[ch] == null) ch++;
            next[i] = buckets[ch].poll();
            if (buckets[ch].isEmpty()) ch++;
        }

        //Traverse next
        while (lng > 0) { //handle repeats
            for ( int idx = next[firstIdx]; idx != firstIdx; idx = next[idx] ) {
                BinaryStdOut.write(input.charAt(idx));
                lng--;
            }
            BinaryStdOut.write(input.charAt(firstIdx));
            lng--;
        }
        BinaryStdOut.close();
    }

    // if args[0] is '-', apply Burrows-Wheeler encoding
    // if args[0] is '+', apply Burrows-Wheeler decoding
    public static void main(String[] args) {
        if      (args[0].equals("-")) encode();
        else if (args[0].equals("+")) decode();
        else throw new IllegalArgumentException("Illegal command line argument");
    }
}

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.BinaryStdIn;


public class CircularSuffixArray {
    // alphabet size of extended ASCII
    private static final int R = 256;
    
    private final int lng;
    private final int[] idx;
        
    private char getChar( String s, int offset ) {
        char ch = s.charAt(offset % lng);
//StdOut.println("DEBUG: " + "getChar( " + offset + ") =" + ch); 
        return ch;
    }

    //MSD sort
    private void sortIdx( String s, int beg, int end, int offset ) {
        class StackItem {
            int beg;
            int end;
            int offset;
            StackItem parent;
            StackItem ( int b, int e, int o ) {
                beg = b;
                end = e;
                offset = o;
            }
        }
        class Stack {
            StackItem top;
            
            Stack ( int b, int e, int o ) {
                top = new StackItem( b, e, o );
            }
            
            void push ( int b, int e, int o ) {
//StdOut.println("DEBUG : " + "push(" + b + "," + e + "," + o + ")");
                StackItem it = new StackItem( b, e, o );
                it.parent = top;
                top = it;
            }
            
            StackItem pop () {
                StackItem res = top;
//StdOut.println("DEBUG : " + "pop(" + res.beg + "," + res.end + "," + res.offset + ")");
                top = top.parent;
                return res;
            }
            
            boolean isEmpty () {
                return top == null;
            }
        }
        
        Stack stack = new Stack (beg, end, offset);
        
        while (!stack.isEmpty()) {
            StackItem it = stack.pop();
            if (it.end <= it.beg || it.offset >= lng) continue;
            int lt = it.beg, gt = it.end;
            int v = getChar(s, idx[lt] + it.offset);
            int i = it.beg + 1;
//StdOut.println("DEBUG: " + "sortIdx( " + s + " , " + it.beg + " , " + it.end + " , " + it.offset + " ):" + (char)v + "\n\t" + getString());
            while (i <= gt) {
//StdOut.println( "\t\t" + i + " : " + idx[i] + " : " + getChar(s, idx[i] + it.offset) + " " + lt + " " + gt);
                int t = getChar(s, idx[i] + it.offset);
                if     (t < v) {
                    int ti = idx[lt];
                    idx[lt] = idx[i];
                    idx[i] = ti;
                    lt++;
                    i++;
                } else if ( t > v ) {
                    int ti = idx[gt];
                    idx[gt] = idx[i];
                    idx[i] = ti;
                    gt--;
                } else i++;
//StdOut.printf("\t\t\t");
//for (int k = it.beg; k < it.end; k++) StdOut.printf(" %c ", getChar(s, idx[k] + it.offset));
//StdOut.println("");
            }
//StdOut.println("<--\t" + getString() + " : " + lt + " " + gt);
            if (gt - lt >= 1) stack.push(lt, gt, it.offset + 1);
            if ( lt - it.beg > 1 ) stack.push( it.beg, --lt , it.offset );
            if ( it.end - gt > 1 ) stack.push( ++gt, it.end , it.offset );
//StdOut.println("<--\t" + getString());
        }
    }

    // circular suffix array of s
    public CircularSuffixArray(String s) {
        lng = s.length();        
        idx = new int[lng];
        for (int i = 0; i < lng; i++) idx[i] = i;
        
        if (lng == 0) return;

        sortIdx( s, 0, lng - 1, 0 );
    }
    // length of s
    public int length() {
        return lng;
    }
    // returns index of ith sorted suffix
    public int index(int i) {
        return idx[i];
    }
    
    private String getString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lng; i++)
            sb.append(idx[i] + " ");
        return sb.toString();
    }
    // unit testing of the methods (optional)
    public static void main(String[] args) {
        String str = BinaryStdIn.readString();
        CircularSuffixArray csa = new CircularSuffixArray(str);
        StdOut.println(csa.getString());
    }
}

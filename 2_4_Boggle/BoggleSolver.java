import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

import java.util.Arrays;
import java.util.Queue;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.HashSet;


public class BoggleSolver
{
    private static final int NOT_FOUND = 0;
    private static final int PREF_FOUND = 1;
    private static final int WORD_FOUND = 2;
    
    private class Dictionary {
        private final int maxWordLength;
        private final int eow;
        private final RwayTrie trie;

        public Dictionary (String[] words) {
            eow = 1 << alphabetSize;

            //Collect statistics
//            int[] lengthStats = new int[MAX_WORD_LENGTH];
//            String prev = "";
//            boolean isSorted = true;
            int maxLng = 0;
            for (int i = 0; i < words.length; i++) {
//                if (words[i].compareTo(prev) < 0) isSorted = false;
                int lng = words[i].length();
//                if (lng > MAX_WORD_LENGTH - 1) continue;
                if (lng > maxLng) maxLng = lng;
//                lengthStats[lng]++;
//                prev = words[i];
            }
            maxWordLength = maxLng;
            int trieDepth = maxLng;
//StdOut.println("Input dictionary is " + (isSorted ? "" : "NOT ") + "sorted"); 
//StdOut.println("MAX LENGTH: " + maxWordLength);
//for (int i = 0; i <= trieDepth; i++)
//StdOut.println("\t" + (maxWordLength - i) + " :\t" + lengthStats[maxWordLength - i] + "\t");
                

            trie = new RwayTrie (maxWordLength + 1);
             
            class Word {
                private int tailIdx;
                private final String word;
                private final int length;
                public Word( String w ) {
                    tailIdx = 0;
                    word = w;
                    length = w.length();
                }
                public char charAt( int i ) {
                    return word.charAt(i);
                }
            }
             
            final LinkedList<Word> wordList = new  LinkedList<Word>();
            for (String str : words) {
                assert (wordList.isEmpty() 
                            || str.compareTo(wordList.getLast().word) > 0);
                Word word = new Word( str );
                if (word.length < MIN_WORD_LENGTH) continue;
                wordList.add(word);
            }
            
            for (int l = 0; l <= trieDepth; l++) {
                final Iterator<Word> it = wordList.iterator();
                while (it.hasNext()) {
                    final Word word = it.next();
                    if (word.length == l) {
                        trie.addEOW( l, word.tailIdx );
                        it.remove();
                        continue;
                    }
                    final int ch = alphabet[word.charAt(l)];
                    final int n = word.tailIdx;
                    trie.add( l, n, ch );
                    word.tailIdx = trie.getSuccessor( l, n, ch );
//StdOut.println("DEBUG: " + "FillIn " + word + " " + l + " " + n + " " + ch + " " +  word.tailIdx);
                }
            }
//        StdOut.println("DEBUG : " + "There are " + prefixTries.length 
//                        + " trie slots and " +  leafs + " leafs");
        }
                       
/*
        private boolean add( char[] word ) {
            final int idx = getIndex( word );
            if (prefixTries[idx] == null) prefixTries[idx] = new RwayTrie();
            final RwayTrie trie = prefixTries[idx];
            int l = 0;
            int n = 0;
            int k = prefixSize;
            final int depth = word.length - prefixSize;
            while ( l < depth ) {
                final int ch = alphabet[word[k++]];
                trie.add( l, n, ch );
                n = trie.getSuccessor( l, n, ch );
                l++; 
            }
            return trie.addEOW( l, n );
        }
*/
        public int isThere( char[] word ) {
             int res = NOT_FOUND;
//StdOut.println("DEBUG: " + "isThere (" + (new String(word)) + " )");
            int l = 0;
            int n = 0;
            final int depth = word.length;
            while ( l < depth ) {
                final int ch = alphabet[word[l]];
                if (!trie.hasSuccessor( l, n, ch )) return NOT_FOUND;
                n = trie.getSuccessor( l, n, ch );
                l++; 
            }
            if (trie.hasEOW( l, n )) res |= WORD_FOUND;
            if (!trie.isLast( l, n )) res |= PREF_FOUND;
            return res;
        }
        
        public int isThereHint( char[] word, int l, int[] nStack ) {
            int res = NOT_FOUND;
//StdOut.println ("DEBUG : " + " isThereHint : " + String.valueOf(word, 0, l + 1) + " " + l + " " + nStack[l]); 
            final int n  = nStack[l];
            final int ch = alphabet[word[l]];

//StdOut.println ("DEBUG : " + " isThereHint :\t" + " " + n + " " + ch); 
            if (trie.hasSuccessor( l, n, ch )) {
                final int succ = trie.getSuccessor( l++, n, ch );
                if (!trie.isLast( l, succ )) {
                        res |= PREF_FOUND;
                        nStack[ l ] = succ;
                    }
                if (trie.hasEOW( l, succ )) { 
                    res |= WORD_FOUND;
                    nStack[ l ] = succ;
                }
//StdOut.println ("DEBUG : " + " isThereHint :\t\t" + l + " " + n + " " + ch + " " + succ); 
            }
//StdOut.println ("DEBUG : " + " isThereHint :\t\t\t" + res); 
            return res;
        }
        
        public void deleteHint( char[] word, int l, int[] nStack ) {
//StdOut.println ("DEBUG : " + " DeleteHint " + " " + String.valueOf(word) + " " + l + " " + Arrays.toString(nStack)); 
            int n = nStack[l];
            assert (trie.hasEOW( l, n ));
            trie.delEOW(l, n);
            while (trie.isEmpty(l, n)) {
                n = nStack[--l];
                trie.del(l, n, word[l]);
            }
//StdOut.println(toString());
        
        }
                    
        public String toString() {
            return trie.toString();
        }

        private class RwayTrie {
            private final long[][] storage;
            private int nPrev;
            private final boolean[] hasChars;
            private int[] lengthSumStats;
        
            public RwayTrie(int trieDepth) {
                storage = new long[trieDepth][];
                hasChars = new boolean[trieDepth];
                lengthSumStats = new int[trieDepth];
                lengthSumStats[0] = 1;
                allocateStorage(0);
            }

            private boolean add( int l, int n, int ch ) {
                addHelper( l, n );
                long bit = (1L << ch);
                if ( (storage[l][n] & bit) == bit ) return false;
                storage[l][n] |= bit;
                lengthSumStats[ l + 1 ]++;
//StdOut.println("ADD: " + l + " " + n + " " + ch + " " + storage[l][n] + " " + lengthSumStats[ l + 1 ]);
                return true;
            }

            private boolean addEOW( int l, int n ) {
//StdOut.println("ADD: " + l + " " + n + " EOW" );
                if (storage[l] == null) allocateStorage( l );
                if ( hasEOW( l, n ) ) return false;
                storage[l][n] |= eow;
                return true;
            }
            
            private void del( int l, int n, int ch ) {
//StdOut.println("DEL: " + l + " " + n + " " + ch + " " + storage[l][n] + " ");
                storage[l][n] &= ~(1L << ch);            
            }
            private void delEOW( int l, int n ) {
//StdOut.println("DEL: " + l + " " + n + " EOW" );
                storage[l][n] &= ~eow;
            }
            private void allocateStorage( int l ) {
                int size = lengthSumStats[l];
                storage[l] = new long[size];
//StdOut.println("DEBUG: " + "Allocating " + 8*size + " bytes, l = " + l
//                    +" leafs = " + leafs );
                }

            private void addHelper( int l, int n ) {
                assert (l == 0 || storage[l-1] != null);
                assert ( n >= nPrev || hasChars[l] == false);
                try {
                    if (storage[l] == null) allocateStorage( l );
                    if (!hasChars[l]) { // the very first item
                        nPrev = -1;
                        hasChars[l] = true;
                    } else if ((storage[l][n] & ~eow) == 0) {//set offset
                        long prevItem = storage[l][nPrev];
                        long offset = prevItem >> 32;
                        int chars = ((int)prevItem) & ~eow;
                        offset += getBitsCount(chars);
                        storage[l][n] |= (offset << 32);
//StdOut.println("OFFSET : " + l + " " + n + " " + storage[l][n] + " " 
//                                + prevItem + " " + offset + " " + chars); 
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage() + " DEBUG: " + l + " " 
                                + n + " " + " " + storage[l].length);
                }
                nPrev = n;
            }
            //Search helpers

            //returns true if there is a prefix with character ch in the dict
            public boolean hasSuccessor( int l, int n, int ch ) { 
                long bit = (1L << ch);
                return (storage[l][n] & bit) == bit;
            }
        
            public boolean hasEOW( int l, int n ) { 
                return (storage[l][n] & eow) == eow;
            }
            
            public boolean isLast( int l, int n ) {
                return (storage[l][n] & ~eow) == 0;
            }
             public boolean isEmpty( int l, int n) {
                return storage[l][n] == 0;
            }
       
            //get sucessor's index (DOES NOT check if successor exists)
            public int getSuccessor( int l, int n, int ch ) {
                //first 32-bit word of storage[l][n] is the index of the begining
                //offset is the number of set bits of the storage's alphabet part
                final long item = storage[l][n];
                final int begin = (int)(item >> 32);
                final int chars = ((int)item) & ~eow;
                final int res = begin + getBitsCount(chars >> (ch+1));
//StdOut.println("DEBUG: " + "getSuccessor " + item + " " + begin + " " + chars + " " + res);
                return res;
            }
            private int getBitsCount(int v) {
                v = v - ((v >> 1) & 0x55555555);     // reuse input as temporary
                v = (v & 0x33333333) + ((v >> 2) & 0x33333333);     // temp
                return ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24; // count
            }

            
            public String toString() {
                StringBuilder sb = new StringBuilder();
                for (int l = 0; l < storage.length; l++) {
                    if (storage[l] == null) {
                        sb.append(" NULL ");
                        continue;
                    }
                    for (int n = 0; n < storage[l].length; n++) {
                        if (storage[l][n] != 0) {
                            sb.append(" ");
                            sb.append(n);
                            sb.append(":");
                        } 
                        long val = storage[l][n] & ~eow;
                        if (val != 0) sb.append(val);
                        if (hasEOW( l, n )) sb.append( "!" );
                    }
                    sb.append("|");
                }
                return sb.toString();
            }
        }
    }
    private final Dictionary dictionary;
    
    private static final int MAX_WORD_LENGTH = 50;
    private static final int MIN_WORD_LENGTH = 3;
    private static final int alphabetSize = 26;
    private static final int alphabetShift = 64 - alphabetSize;
    private static final int[] alphabet = { 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x0F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x1F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x2F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x3F
        -1, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, //0x4F 'O'
        10,  9,  8,  7,  6,  5,  4,  3,  2,  1,  0, -1, -1, -1, -1, -1, //0x5F '_'
        -1, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, //0x6F 'o'
        10,  9,  8,  7,  6,  5,  4,  3,  2,  1,  0, -1, -1, -1, -1, -1, //0x7F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x8F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0x9F
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xAF
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xBF
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xCF
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xDF
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xEF
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, //0xFF
        };
    // Initializes the data structure using the given array of strings as 
    // the dictionary. 
    //(You can assume each word in the dictionary contains only 
    // the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {
        this.dictionary = new Dictionary(dictionary);
    }

    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {
        LinkedList<String> result = new LinkedList<String>();
        DFSstack stack = new DFSstack( board );
        if (stack.word.length < MIN_WORD_LENGTH) return result;
        int[] nStack = new int[dictionary.maxWordLength + 1];
        
        for (int root = 0; root < stack.NM; root++ ) {
            stack.pushFirst( root );
            while ( !stack.isEmpty() ) {
                int isThere = PREF_FOUND;
                int let = stack.length - stack.getTopLetNum();
                while (let < stack.length-1 ) {
                    if ((dictionary.isThereHint( stack.word, let++, nStack ) 
                            & PREF_FOUND) != PREF_FOUND) {
                        isThere = NOT_FOUND;
                        break;
                    }
                }
                if ((isThere & PREF_FOUND) == PREF_FOUND) 
                    isThere = dictionary.isThereHint( stack.word, let, nStack );
                if ((isThere & WORD_FOUND) == WORD_FOUND) {
                    String word = stack.getStr();
//StdOut.println("DEBUG: " + "WORD FOUND: " + new String(word));
                    result.add( word );
                    dictionary.deleteHint( stack.word, ++let, nStack ); 
                }
                if ((isThere & PREF_FOUND) != PREF_FOUND || !stack.pushAdjTop())
                    stack.popRec();

//StdOut.println("DEBUG: " + " DFS " + stack.top + " " + isThere + " " + (String.valueOf(stack.word, 0, let + 1)) + " " + let);
            }
        }
        
        return result;
    }
    private class DFSstack {
        private final int NM;
        
        private char[] word;
        private int length;
        
        private int level;
        private int top;
        private int[] stack;
        private boolean[] inPath;
        
        private char[][] dices;
        private int adjList[][];
        
        private void reset() {
            top = -1;
            length = 0;
            level = -1;
        }
        
        public DFSstack( BoggleBoard board ) {
            final int M = board.cols();
            final int N = board.rows();
            NM = N*M;
            assert (N > 0 && M > 0);
            
            word = new char[2*NM];
            
            stack = new int[8*NM - 2*N - 2*M - 4];
            inPath = new boolean[NM];
            
            dices = new char[NM][];
            int k = 0;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    char letter = board.getLetter( i, j );
                    char[] dice; 
                    if (letter == 'Q') {
                        dice = new char[2];
                        dice[1] = 'U';
                    } else dice = new char[1];
                    dice[0] = letter;
                    dices[k++] = dice;
                }
            }
            
            reset();
            
            adjList = new int[NM][];
            if (M == 1 || N == 1) {
                if (NM == 1) {
                    adjList[0] = new int[0];
                    return;
                }
                adjList[0] = new int[1];
                adjList[0][0] = 1;
                for (k = 1; k < (NM - 1); k++) {
                    adjList[k] = new int[2];
                    adjList[k][0] = k - 1;
                    adjList[k][1] = k + 1;
                }
                adjList[k] = new int[1];
                adjList[k][0] = k - 1;
                assert (++k == NM);
                return;
            }
            
            adjList = new int[NM][];
            adjList[0] = new int[3];
            adjList[0][0] = 1;
            adjList[0][1] = M + 1;
            adjList[0][2] = M;
            for (int j = 1; j < (M - 1); j++) {
                adjList[j] = new int[5];
                adjList[j][0] = j + 1;
                adjList[j][1] = M + j + 1;
                adjList[j][2] = M + j;
                adjList[j][3] = M + j - 1;
                adjList[j][4] = j - 1;
            } 
            adjList[M - 1] = new int[3];
            adjList[M - 1][0] = M - 1 + M;
            adjList[M - 1][1] = M + M - 2;
            adjList[M - 1][2] = M - 2;

            k = M;
            for (int i = 1; i < (N - 1); i++) {
                adjList[k] = new int[5];
                adjList[k][0] = k - M;
                adjList[k][1] = k - M + 1;
                adjList[k][2] = k + 1;
                adjList[k][3] = k + M + 1;
                adjList[k][4] = k + M;
                k++;
                for (int j = 1; j < (M - 1); j++) {
                    adjList[k] = new int[8];
                    adjList[k][0] = k - M + 1;
                    adjList[k][1] = k + 1;
                    adjList[k][2] = k + M + 1;
                    adjList[k][3] = k + M;
                    adjList[k][4] = k + M - 1;
                    adjList[k][5] = k - 1;
                    adjList[k][6] = k - M - 1;
                    adjList[k][7] = k - M;
                    k++;
                }
                adjList[k] = new int[5];
                adjList[k][0] = k + M;
                adjList[k][1] = k + M - 1;
                adjList[k][2] = k - 1;
                adjList[k][3] = k - M - 1;
                adjList[k][4] = k - M;
                k++;
            }

            adjList[k] = new int[3];
            adjList[k][0] = k - M;
            adjList[k][1] = k - M + 1;
            adjList[k][2] = k + 1;
            k++;
            for (int j = 1; j < (M - 1); j++) {
                adjList[k] = new int[5];
                adjList[k][0] = k - 1;
                adjList[k][1] = k - 1 - M;
                adjList[k][2] = k - M;
                adjList[k][3] = k + 1 - M;
                adjList[k][4] = k + 1;
                k++;
            } 
            adjList[k] = new int[3];
            adjList[k][0] = k - 1;
            adjList[k][1] = k - 1 - M;
            adjList[k][2] = k - M;
            
            assert (++k == NM);
            
//StdOut.println(printAdjList());
        }
        
        public int getTopLetNum() {
            return dices[stack[top]].length;
        }
        
        public String printAdjList() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < adjList.length; i++) {
                sb.append(i + ":\t");
                for (int j = 0; j < adjList[i].length; j++) {
                    sb.append(adjList[i][j] + "\t");
                }
                sb.append("\n");
            }
            return sb.toString();
        }
        public void pushFirst( int r ) {
            assert (r >= 0 && r < NM);
            assert (top == -1);
            stack[++top] = r;
            addTopToPath();
        } 
        
        private boolean pushAdj( int r ) {
            for ( int k : adjList[r] ) if (!inPath[k]) stack[++top] = k;
            if (inPath[stack[top]]) return false;
            addTopToPath();
//StdOut.println("DEBUG: "  + "pushAdj(" + r + ") : "  + level + " " + top + " " + getStr() + " " + length);
//StdOut.println("DEBUG: " + "stack : " + Arrays.toString(stack) );
//StdOut.println("DEBUG: " + "inPath : " + Arrays.toString(inPath) );  
            return true;
        }
        
        public boolean pushAdjTop () {
            return pushAdj(stack[top]);
        }
            
        public boolean isEmpty() {
            return top < 0;
        }
        
        public void popRec() {
            while (popIfInPath());
            if (top >= 0) addTopToPath();
//StdOut.println("DEBUG: "  + "popRec(): " + level + " " + top + " " + " " + getStr() + " " + length);
//StdOut.println("DEBUG: " + "stack : " + Arrays.toString(stack) );
//StdOut.println("DEBUG: " + "inPath : " + Arrays.toString(inPath) );  
        }
        
        private boolean popIfInPath() {
            if (top < 0) return false;
            final int k = stack[top];
            if (!inPath[k]) return false;
            inPath[k] = false;
            level--;
            top--;
            length -= dices[k].length;
//StdOut.println("DEBUG: "  + "popIfInPath(): " + level + " " + top + " " + " " + getStr() + " " + length);
//StdOut.println("DEBUG: " + "stack : " + Arrays.toString(stack) );
//StdOut.println("DEBUG: " + "inPath : " + Arrays.toString(inPath) );  
            return true;
        }
        
        private void addTopToPath() {
            assert (top >= 0);
            final int k = stack[top];
            assert (!inPath[k]);
            inPath[k] = true; 
            ++level;
            final char[] dice = dices[k];
            assert (dice.length > 0);
            for (int i = 0; i < dice.length; i++)
                word[length++] = dice[i];
        }
        
        public String getStr() {
            return String.valueOf(word, 0, length);
        }
    }

    //Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        int length = word.length();
        int n = 0;
        int l = 0;
        int offset = 0;
        final char [] encoded = word.toCharArray();
        
        if ((dictionary.isThere(encoded) & WORD_FOUND) == WORD_FOUND) 
            return length2score(length);
        return 0;
    }
    private static int length2score(int lng) {
        if (lng < 3) return 0;
        if (lng > 7) return 11;
        return scoreTable[lng];
    }
    private static int[] scoreTable = { 0, 0, 0, 1, 1, 2, 3, 5 };
        
    public static void main(String[] args)
    {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
//        StdOut.println(solver.dictionary.toString());

        BoggleBoard board = new BoggleBoard(args[1]);
/*
        for (int i = 0; i < 10000; i++)
            solver.getAllValidWords(board);
*/

        int score = 0;
        int i = 0;
        for (String word : solver.getAllValidWords(board))
        {
            final int scr = solver.scoreOf(word);
            StdOut.println( ++i + "\t" + scr + "\t" + word);
            score += scr;
            assert (Arrays.binarySearch(dictionary, word)>=0);
        }
        StdOut.println("Score = " + score);
        
    }
}
